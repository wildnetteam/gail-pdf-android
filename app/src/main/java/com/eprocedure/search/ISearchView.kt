package com.eprocedure.search

import com.eprocedure.login.model.response.LoginResponse

interface ISearchView {
    fun showProgress()
    fun hideProgress()
    fun setItemsSearch(items: LoginResponse)
    fun onHomeFailure(failureMessage: String)
}