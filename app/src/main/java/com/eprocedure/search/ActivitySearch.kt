package com.eprocedure.search

import android.app.Activity
import android.app.ProgressDialog
import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import com.eprocedure.R
import com.eprocedure.button.IButtonView
import com.eprocedure.button.presenter.ButtonPresenterImpl
import com.eprocedure.content.ContentDetail
import com.eprocedure.login.model.request.LoginRequest
import com.eprocedure.login.model.response.Datum
import com.eprocedure.login.model.response.LoginResponse
import com.eprocedure.search.controller.SearchAdapter
import com.eprocedure.search.presenter.SearchPresenterImpl
import com.eprocedure.utility.UIUtils
import kotlinx.android.synthetic.main.activity_search.*
import java.util.*

class ActivitySearch : Activity(), ISearchView , IButtonView {
    private var position: Int = 0

    override fun setItems(items: LoginResponse) {
        if (items.data!!.size > 0) {
            data!!.clear()
            data!!.addAll(items.data!!)
            adapter.notifyDataSetChanged()
        } else {
            val intent = Intent(applicationContext, ContentDetail::class.java)
            intent.putExtra("data", items)
            intent.putExtra("header", "Search")
            intent.putExtra("all_data", data)
            intent.putExtra("user_id", user_id)
            intent.putExtra("heading_name", heading_name)
            intent.putExtra("parent_term_id", parent_term_id)
            intent.putExtra("search_text", search_text)
            intent.putExtra("position", position)
            startActivity(intent)
        }
    }
    private var pdf_url: String? = ""

    private var user_id: Int = 0
    lateinit var mainPresenter: SearchPresenterImpl
    private lateinit var adapter: SearchAdapter
    private var data: ArrayList<Datum>? = ArrayList()
    private var progress: ProgressDialog? = null
    lateinit var buttonPresenter: ButtonPresenterImpl

    override fun showProgress() {
        progress = ProgressDialog(this)
        progress!!.setMessage("Loading")
        progress!!.setCancelable(false)
        progress!!.show()
    }

    override fun hideProgress() {
        if (progress!=null){

            progress!!.dismiss()
        }
    }

    override fun setItemsSearch(items: LoginResponse) {
        data!!.clear()
        data!!.addAll(items.data!!)
        adapter.notifyDataSetChanged()

        if (data!!.size>0){
            content_rv.visibility= View.VISIBLE
            no_data_tv.visibility= View.GONE
        }else{
            content_rv.visibility= View.GONE
            no_data_tv.visibility= View.VISIBLE
        }
    }
    override fun onHomeFailure(failureMessage: String) {
    }
    private var heading_name: String = ""
    private var parent_term_id: Int = 0
    var search_text: String = ""
    lateinit var headerString: String
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_search)
        user_id = intent.extras.getInt("user_id")
        heading_name = intent.extras.getString("heading_name")
        search_text = intent.extras.getString("search_text")
        pdf_url = intent.extras.getString("pdf_url")
        parent_term_id = intent.extras.getInt("parent_term_id")
        buttonPresenter = ButtonPresenterImpl(this)
        val mLoginReuest = LoginRequest()
        mLoginReuest.deviceId = UIUtils.getUniqueId(this@ActivitySearch)
        mLoginReuest.userId = user_id
        mLoginReuest.term_id=parent_term_id
        mLoginReuest.search_text = search_text
        mainPresenter = SearchPresenterImpl(this@ActivitySearch)
        mainPresenter.getsearchData(mLoginReuest)
        setAdapter()
    }
    private fun setAdapter() {
        adapter = SearchAdapter(data!!, this@ActivitySearch, object : SearchAdapter.ItemClickInterfaces {
            override fun onitemClick(mdatum: Datum,posi : Int) {
                position=posi
                val mLoginReuest = LoginRequest()
                mLoginReuest.deviceId = UIUtils.getUniqueId(this@ActivitySearch)
                mLoginReuest.userId = user_id
                mLoginReuest.term_id = mdatum.termId
                mLoginReuest.search_text =search_text
                headerString = mdatum.name!!
                buttonPresenter.getHomeData(mLoginReuest)
            }
        })
        val layoutManager = LinearLayoutManager(applicationContext)
        content_rv?.layoutManager = layoutManager
        content_rv?.adapter = adapter
        adapter.notifyDataSetChanged()
    }
}