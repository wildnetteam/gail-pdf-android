package com.eprocedure.search.controller

import android.content.Context
import android.graphics.Color
import android.support.v7.widget.RecyclerView
import android.text.Spannable
import android.text.SpannableStringBuilder
import android.text.style.ForegroundColorSpan
import android.util.TypedValue
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.eprocedure.R
import com.eprocedure.login.model.response.Datum
import com.eprocedure.search.ActivitySearch
import com.eprocedure.utility.PreferenceHelper
import java.util.*


class SearchAdapter(private var items: ArrayList<Datum>, val context: Context, private val btnlistener: ItemClickInterfaces) : RecyclerView.Adapter<SearchAdapter.ViewHolder>() {
    companion object {
        var itemClickListener: ItemClickInterfaces? = null
    }
    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): ViewHolder {
        val itemView = LayoutInflater.from(parent.context)
                .inflate(R.layout.content_item_view, parent, false)
        return ViewHolder(itemView)
    }
    override fun onBindViewHolder(holder: ViewHolder, p1: Int) {
        itemClickListener = btnlistener
        val userDto = items[p1]
        if (userDto.updated.equals("1")) {
            holder.homeListTitle?.setTextColor(Color.parseColor("#FF0000"))
        }
        var sb = SpannableStringBuilder(userDto.name)
        if (((context as ActivitySearch).search_text.isNotEmpty())) {
            var index = userDto.name!!.indexOf((context.search_text))
            while (index != -1) {
                sb = SpannableStringBuilder(userDto.name)
                val fcs = ForegroundColorSpan(Color.rgb(124, 252, 0)) //specify color here
                sb.setSpan(fcs, index, index + ((context).search_text.length), Spannable.SPAN_INCLUSIVE_INCLUSIVE)
                index = userDto.name!!.indexOf(context.search_text, index + 1)
            }
            holder.homeListTitle?.text = sb
        } else {
            holder.homeListTitle?.text = userDto.name
            holder.homeListTitle?.setTextColor(Color.parseColor("#000000"))
        }
        val size = PreferenceHelper.getMyPrefString(context, "size")
        if (size.equals("") || size!!.toLowerCase() == "normal") {
            holder.homeListTitle!!.setTextSize(TypedValue.COMPLEX_UNIT_SP, 18f)
        } else if (size.toLowerCase() == "large") {
            holder.homeListTitle!!.setTextSize(TypedValue.COMPLEX_UNIT_SP, 20f)
        } else {
            holder.homeListTitle!!.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16f)
        }
        holder.homeListTitle?.setOnClickListener { itemClickListener?.onitemClick(userDto, p1) }
    }
    override fun getItemCount(): Int {
        return items.size
    }
    class ViewHolder(row: View) : RecyclerView.ViewHolder(row) {
        var homeListTitle: TextView? = null

        init {
            this.homeListTitle = row.findViewById(com.eprocedure.R.id.home_list_title)
        }
    }
    interface ItemClickInterfaces { // create an interface
        fun onitemClick(mdatum: Datum, position: Int)  // create callback function
    }
}