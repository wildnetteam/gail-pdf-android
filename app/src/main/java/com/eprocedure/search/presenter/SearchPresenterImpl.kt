package com.eprocedure.search.presenter

import com.eprocedure.login.model.request.LoginRequest
import com.eprocedure.login.model.response.LoginResponse
import com.eprocedure.search.ISearchView
import com.eprocedure.search.interactor.SearchInteractor
import com.eprocedure.search.interactor.SearchInteractorImpl

class SearchPresenterImpl(private val mIHomeView: ISearchView?) : SearchPresenter, SearchInteractor.OnFinishedListener {
    override fun getsearchData(mLoginRequest: LoginRequest) {
        mIHomeView!!.showProgress()
        mHomeInteractor.getsearchData(this, mLoginRequest)
    }

    override fun onDestroy() {
    }

    override fun onsearchSucess(items: LoginResponse) {
        mIHomeView?.hideProgress()
        mIHomeView?.setItemsSearch(items)
    }

    override fun onsearchFailed(errorMessage: String) {
        if (mIHomeView != null) {
            mIHomeView.hideProgress()
            mIHomeView.onHomeFailure(errorMessage)

        }
    }


    private val mHomeInteractor: SearchInteractor

    init {
        this.mHomeInteractor = SearchInteractorImpl()

    }


}
