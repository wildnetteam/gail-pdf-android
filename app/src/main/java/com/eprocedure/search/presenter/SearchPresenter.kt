package com.eprocedure.search.presenter

import com.eprocedure.login.model.request.LoginRequest

interface SearchPresenter{
    fun getsearchData(mLoginRequest: LoginRequest)

    fun onDestroy()

}