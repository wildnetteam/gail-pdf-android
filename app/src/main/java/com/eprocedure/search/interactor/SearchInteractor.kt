package com.eprocedure.search.interactor

import com.eprocedure.login.model.request.LoginRequest
import com.eprocedure.login.model.response.LoginResponse

interface SearchInteractor {
    fun getsearchData(listener: OnFinishedListener, model: LoginRequest)
    interface OnFinishedListener {
        fun onsearchSucess(items: LoginResponse)
        fun onsearchFailed(errorMessage: String)

    }
}