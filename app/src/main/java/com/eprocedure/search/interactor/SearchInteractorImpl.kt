package com.eprocedure.search.interactor

import com.eprocedure.login.model.request.LoginRequest
import com.eprocedure.login.model.response.LoginResponse
import com.eprocedure.networking.retrofit.client.ApiFactory
import rx.Subscriber
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers

class SearchInteractorImpl : SearchInteractor {
    override fun getsearchData(listener: SearchInteractor.OnFinishedListener, model: LoginRequest) {
        ApiFactory.getClient().getsearchData(model.userId, model.deviceId, model.term_id, model.search_text).subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : Subscriber<LoginResponse>() {
                    override fun onCompleted() {}
                    override fun onError(e: Throwable) {
                        listener.onsearchFailed("Not able to connect")
                    }

                    override fun onNext(mLoginResponse: LoginResponse) {
                        if (mLoginResponse.status == 200) {
                            listener.onsearchSucess(mLoginResponse)
                        } else {
                            listener.onsearchFailed(mLoginResponse.error!!)
                        }
                    }
                })
    }
}