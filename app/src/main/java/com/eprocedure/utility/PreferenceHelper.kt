package com.eprocedure.utility

import android.content.Context

object PreferenceHelper {

    fun getMyPrefString(context: Context, key : String): String? {
        val preference = context.getSharedPreferences("myPref", Context.MODE_PRIVATE)
        return preference.getString(key,"")
    }

    fun setMyPrefString(context: Context, key : String,value : String) {
        val preference = context.getSharedPreferences("myPref", Context.MODE_PRIVATE)
        val editor = preference.edit()
        editor.putString(key, value)
        editor.apply()
    }


    fun getMyPrefBoolean(context: Context, key : String): Boolean? {
        val preference = context.getSharedPreferences("myPref", Context.MODE_PRIVATE)
        return preference.getBoolean(key,false)
    }

    fun setMyPrefBoolean(context: Context, key : String,value : Boolean) {
        val preference = context.getSharedPreferences("myPref", Context.MODE_PRIVATE)
        val editor = preference.edit()
        editor.putBoolean(key, value)
        editor.apply()
    }
}
