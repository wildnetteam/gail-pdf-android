package com.eprocedure.utility

import android.annotation.SuppressLint
import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.provider.Settings
import android.text.Html
import android.text.Spanned


class UIUtils{

    /**
     * Set specific color to string
     *
     * @return Colored Spanned word
     */

    companion object {
        fun getYoungestTextColor(): Spanned {
            val first = AppConstant.APP_TAG_INDIA
            val next = "<font color='#00BCD4'>" + AppConstant.APP_TAG_YOUNGEST + "</font>" + AppConstant.APP_TAG_MAHARATNA
            return Html.fromHtml(first + next)
        }

        @SuppressLint("HardwareIds")
        fun getUniqueId(context: Context): String {
            return Settings.Secure.getString(context.contentResolver,
                    Settings.Secure.ANDROID_ID)
        }

         fun isNetworkAvailable(context: Context):  Boolean
        {
            val cm = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            var activeNetworkInfo: NetworkInfo? = null
            activeNetworkInfo = cm.activeNetworkInfo
            return activeNetworkInfo != null && activeNetworkInfo.isConnectedOrConnecting
        }

    }





}

