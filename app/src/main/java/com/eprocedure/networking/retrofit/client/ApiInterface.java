package com.eprocedure.networking.retrofit.client;

import com.eprocedure.amendment.model.amendmentResponseBean.AmendmentResponse;
import com.eprocedure.faq.question.model.faqresponseBean.FaqResponseBean;
import com.eprocedure.login.model.response.LoginResponse;

import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Query;
import rx.Observable;

/**
 * Created by Pawan on 9/13/16.
 */
public interface ApiInterface {

    @FormUrlEncoded
    @POST("login")
    Observable<LoginResponse> userLogin(@Field("username") String Email, @Header("device_id") String Password);

    @GET("home")
    Observable<LoginResponse> userHome(@Header("user_id") Integer user_id, @Header("device_id") String device_id);

    @GET("get_term_list")
    Observable<LoginResponse> termData(@Header("user_id") Integer user_id, @Header("device_id") String device_id, @Query("term_id") Integer term_id, @Query("search_text") String search_text);

    @GET("get_amendment_list")
    Observable<AmendmentResponse> getAmendment(@Header("user_id") Integer user_id, @Header("device_id") String device_id, @Query("term_id") Integer term_id);

    @GET("get_circular_list")
    Observable<AmendmentResponse> getCircular(@Header("user_id") Integer user_id, @Header("device_id") String device_id, @Query("term_id") Integer term_id);

    @GET("get_faq_list")
    Observable<FaqResponseBean> faqData(@Header("user_id") Integer user_id, @Header("device_id") String device_id, @Query("term_id") Integer term_id);

    @GET("search_in_faq")
    Observable<FaqResponseBean> getfaqsearchData(@Header("user_id") Integer user_id, @Header("device_id") String device_id, @Query("term_id") Integer term_id, @Query("search_text") String search_text);

    @GET("search_by_term")
    Observable<LoginResponse> getsearchData(@Header("user_id") Integer user_id, @Header("device_id") String device_id, @Query("term_id") Integer term_id, @Query("search_text") String search_text);


}
