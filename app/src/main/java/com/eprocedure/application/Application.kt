package com.eprocedure.application

import android.app.Application
import android.content.Context

class Application : Application() {

    init {
        instance = this
    }

    companion object {
        private var instance: Application? = null

        fun applicationContext() : Context {
            return instance!!.applicationContext
        }
    }

}