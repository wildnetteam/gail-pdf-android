package com.eprocedure.home;

import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.eprocedure.R;

import java.util.Objects;

public class MyDialogFragment extends DialogFragment {
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_select_font, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        TextView a_TextView = view.findViewById(R.id.dialoge_fragment_a);
        TextView a_plus_TextView = view.findViewById(R.id.dialoge_fragment_a_plus);
        TextView a_minus_TextView = view.findViewById(R.id.dialoge_fragment_a_minus);
        a_TextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogListener dialogListener = (DialogListener) getActivity();
                dialogListener.onFinishEditDialog("normal");
                getDialog().dismiss();
            }
        });
        a_minus_TextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogListener dialogListener = (DialogListener) getActivity();
                dialogListener.onFinishEditDialog("small");
                getDialog().dismiss();
            }
        });
        a_plus_TextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogListener dialogListener = (DialogListener) getActivity();
                dialogListener.onFinishEditDialog("large");
                getDialog().dismiss();
            }
        });
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null) {
            Objects.requireNonNull(dialog.getWindow()).setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        }
    }

    public interface DialogListener {
        void onFinishEditDialog(String inputText);
    }
}