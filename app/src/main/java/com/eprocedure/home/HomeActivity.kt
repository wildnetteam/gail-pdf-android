package com.eprocedure.home

import android.annotation.SuppressLint
import android.app.Activity
import android.app.ProgressDialog

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import android.widget.Toast
import com.eprocedure.R
import com.eprocedure.amendment.AmendmentActivity
import com.eprocedure.amendment.CircularActivity
import com.eprocedure.button.ButtonActivity
import com.eprocedure.content.ContentActivity
import com.eprocedure.faq.question.FaqActivity
import com.eprocedure.home.controller.HomeAdapter
import com.eprocedure.home.presenter.HomePresenterImpl
import com.eprocedure.login.model.request.LoginRequest
import com.eprocedure.login.model.response.Datum
import com.eprocedure.login.model.response.LoginResponse
import com.eprocedure.utility.PreferenceHelper
import com.eprocedure.utility.UIUtils
import kotlinx.android.synthetic.main.activity_home.*
import java.util.*


class HomeActivity : Activity(), View.OnClickListener, IHomeView, MyDialogFragment.DialogListener {
    @SuppressLint("SetTextI18n")
    override fun onFinishEditDialog(inputText: String?) {
        Toast.makeText(this@HomeActivity, "" + inputText, Toast.LENGTH_SHORT).show()
        home_text_size.text = resources.getString(R.string.please_select_text_size) + "  " + inputText
        PreferenceHelper.setMyPrefString(this@HomeActivity, "size", inputText.toString())
    }

    private var progress: ProgressDialog? = null
    private var user_id: Int = 0
    private var positionClicked: Int = 0
    private var parent_term_id: Int = 0
    private var heading_name: String = ""
    override fun setTermData(items: LoginResponse) {
        //  Toast.makeText(this, "DATA SIZE==" + items.data!!.size, Toast.LENGTH_SHORT).show()

        if (items.data!!.isNotEmpty()) {
            if (items.data!![0].showButton.equals("1")) {
                val intent = Intent(applicationContext, ButtonActivity::class.java)
                intent.putExtra("data", items)
                intent.putExtra("pdf_url", homeDataList[positionClicked].uploaded_pdf)
                intent.putExtra("user_id", user_id)
                intent.putExtra("heading_name", heading_name)
                intent.putExtra("parent_term_id", parent_term_id)
                startActivity(intent)
            } else {
                try {
                    val intent = Intent(applicationContext, ContentActivity::class.java)
                    intent.putExtra("data", items)
                    intent.putExtra("pdf_url", homeDataList[positionClicked].uploaded_pdf)
                    intent.putExtra("user_id", user_id)
                    intent.putExtra("heading_name", heading_name)
                    intent.putExtra("parent_term_id", parent_term_id)
                    startActivity(intent)
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
        }
    }

    var adapter: HomeAdapter? = null
    private val homeDataList = ArrayList<Datum>()//Creating an empty arraylist
    lateinit var mainPresenter: HomePresenterImpl
    var content: Int = 0

    override fun showProgress() {
        progress = ProgressDialog(this)
        progress!!.setMessage("Loading")
        progress!!.setCancelable(false)
        progress!!.show()
    }

    override fun hideProgress() {
        progress!!.dismiss()

    }

    override fun setItems(items: LoginResponse) {
        // Toast.makeText(this, "DATA SIZE==" + items.data!!.size, Toast.LENGTH_SHORT).show()
        homeDataList.addAll(items.data!!)
        adapter!!.notifyDataSetChanged()
    }

    override fun onHomeFailure(failureMessage: String) {
    }

    override fun onClick(v: View?) {
    }

    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
        user_id = intent.extras.getInt("user_id")
        setAdapter()
        val mLoginReuest = LoginRequest()
        mLoginReuest.deviceId = UIUtils.getUniqueId(this)
        mLoginReuest.userId = user_id
        mainPresenter = HomePresenterImpl(this)
        mainPresenter.getHomeData(mLoginReuest)

        home_text_size.setOnClickListener {
            val manager = fragmentManager
            val frag = manager.findFragmentByTag("fragment_edit_name")
            if (frag != null) {
                manager.beginTransaction().remove(frag).commit()
            }
            val alertDialogFragment = MyDialogFragment()
            alertDialogFragment.show(manager, "fragment_edit_name")

        }

        val size = PreferenceHelper.getMyPrefString(this@HomeActivity, "size")
        // home_text_size.text = resources.getString(R.string.please_select_text_size) + "  " + size

        dialoge_fragment_a.setOnClickListener {
            PreferenceHelper.setMyPrefString(this@HomeActivity, "size", "normal")
            dialoge_fragment_a.setTextColor(Color.parseColor("#ffffff"))
            dialoge_fragment_a_plus.setTextColor(Color.parseColor("#000000"))
            dialoge_fragment_a_minus.setTextColor(Color.parseColor("#000000"))
        }
        dialoge_fragment_a_minus.setOnClickListener {

            PreferenceHelper.setMyPrefString(this@HomeActivity, "size", "small")
            dialoge_fragment_a.setTextColor(Color.parseColor("#000000"))
            dialoge_fragment_a_plus.setTextColor(Color.parseColor("#000000"))
            dialoge_fragment_a_minus.setTextColor(Color.parseColor("#ffffff"))
        }
        dialoge_fragment_a_plus.setOnClickListener {

            PreferenceHelper.setMyPrefString(this@HomeActivity, "size", "large")
            dialoge_fragment_a.setTextColor(Color.parseColor("#000000"))
            dialoge_fragment_a_plus.setTextColor(Color.parseColor("#ffffff"))
            dialoge_fragment_a_minus.setTextColor(Color.parseColor("#000000"))
        }
    }

    private fun setAdapter() {
        adapter = HomeAdapter(homeDataList, this@HomeActivity, object : HomeAdapter.ItemClickInterfaces {
            override fun onitemClick(mdatum: Datum, position: Int) {
                //  Toast.makeText(this@HomeActivity, "clicked id is--" + mdatum.termId, Toast.LENGTH_SHORT).show()
                when {
                    mdatum.slug.equals("amendments") -> {
                        val intent = Intent(applicationContext, AmendmentActivity::class.java)
                        intent.putExtra("term_id", mdatum.termId)
                        intent.putExtra("user_id", user_id)
                        intent.putExtra("parent_term_id", parent_term_id)
                        intent.putExtra("heading_name", heading_name)

                        startActivity(intent)
                    }
                    mdatum.slug.equals("circulars") -> {
                        val intent = Intent(applicationContext, CircularActivity::class.java)
                        intent.putExtra("term_id", mdatum.termId)
                        intent.putExtra("user_id", user_id)
                        intent.putExtra("parent_term_id", parent_term_id)
                        intent.putExtra("heading_name", heading_name)

                        startActivity(intent)
                    }
                    mdatum.slug.equals("faqs") -> {
                        val intent = Intent(applicationContext, FaqActivity::class.java)
                        intent.putExtra("term_id", mdatum.termId)
                        intent.putExtra("user_id", user_id)
                        intent.putExtra("parent_term_id", parent_term_id)
                        intent.putExtra("heading_name", heading_name)
                        startActivity(intent)
                    }
                    else -> {
                        positionClicked = position
                        parent_term_id = mdatum.termId!!
                        heading_name = mdatum.name!!
                        val mLoginReuest = LoginRequest()
                        mLoginReuest.deviceId = UIUtils.getUniqueId(this@HomeActivity)
                        mLoginReuest.userId = user_id
                        mLoginReuest.term_id = mdatum.termId
                        mainPresenter.getTermData(mLoginReuest)
                    }
                }
            }
        })
        val layoutManager = LinearLayoutManager(applicationContext)
        home_rv?.layoutManager = layoutManager
        home_rv?.adapter = adapter
        adapter!!.notifyDataSetChanged()
    }
}