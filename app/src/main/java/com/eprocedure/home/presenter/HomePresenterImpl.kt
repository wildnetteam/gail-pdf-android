package com.eprocedure.home.presenter

import com.eprocedure.home.IHomeView
import com.eprocedure.home.interactor.HomeInteractor
import com.eprocedure.home.interactor.HomeInteractorImpl
import com.eprocedure.login.model.request.LoginRequest
import com.eprocedure.login.model.response.LoginResponse

class HomePresenterImpl(private val mIHomeView: IHomeView?) : HomePresenter, HomeInteractor.OnFinishedListener {


    private val mHomeInteractor: HomeInteractor

    init {
        this.mHomeInteractor = HomeInteractorImpl()

    }

    override fun getHomeData(mLoginRequest: LoginRequest) {
        mIHomeView!!.showProgress()
        mHomeInteractor.homeData(this, mLoginRequest)
    }

    override fun onDestroy() {
    }

    override fun onHomeSucess(items: LoginResponse) {
        mIHomeView?.hideProgress()
        mIHomeView?.setItems(items)
    }

    override fun onHomeFailed(errorMessage: String) {
        if (mIHomeView != null) {
            mIHomeView.hideProgress()
            mIHomeView.onHomeFailure(errorMessage)

        }
    }

    override fun getTermData(mLoginRequest: LoginRequest) {
        mIHomeView!!.showProgress()
        mHomeInteractor.getTermData(this, mLoginRequest)
    }

    override fun onTermSucess(items: LoginResponse) {
        mIHomeView?.hideProgress()
        mIHomeView?.setTermData(items)
    }

    override fun onTermFailed(errorMessage: String) {
        if (mIHomeView != null) {
            mIHomeView.hideProgress()
            mIHomeView.onHomeFailure(errorMessage)

        }
    }

}
