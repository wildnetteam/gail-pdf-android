package com.eprocedure.home.presenter

import com.eprocedure.login.model.request.LoginRequest

interface HomePresenter{
    fun getHomeData(mLoginRequest: LoginRequest)
    fun getTermData(mLoginRequest: LoginRequest)

    fun onDestroy()

}