package com.eprocedure.home

import com.eprocedure.login.model.response.LoginResponse

interface IHomeView {

    fun showProgress()
    fun hideProgress()
    fun setItems(items: LoginResponse)
    fun setTermData(items: LoginResponse)
    fun onHomeFailure(failureMessage: String)

}
