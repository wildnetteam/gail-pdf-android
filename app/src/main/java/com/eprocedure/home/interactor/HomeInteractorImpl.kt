package com.eprocedure.home.interactor

import com.eprocedure.login.model.request.LoginRequest
import com.eprocedure.login.model.response.LoginResponse
import com.eprocedure.networking.retrofit.client.ApiFactory
import rx.Subscriber
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers

class HomeInteractorImpl : HomeInteractor {


    override fun getTermData(listener: HomeInteractor.OnFinishedListener, model: LoginRequest) {
        if (model.search_text==null){
            model.search_text=""
        }
        ApiFactory.getClient().termData(model.userId, model.deviceId, model.term_id,model.search_text).subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : Subscriber<LoginResponse>() {
                    override fun onCompleted() {}

                    override fun onError(e: Throwable) {
                        //dismissProgress();
                        // Toast.makeText(RegistrationActivity.this, e.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                        listener.onHomeFailed("Not able to connect")

                    }
                    override fun onNext(mLoginResponse: LoginResponse) {
                        //  dismissProgress();
                        if (mLoginResponse.status == 200) {
                            listener.onTermSucess(mLoginResponse)
                        } else {
                            listener.onHomeFailed(mLoginResponse.error!!)
                        }
                    }
                })

    }

    override fun homeData(listener: HomeInteractor.OnFinishedListener, model: LoginRequest) {
        ApiFactory.getClient().userHome(model.userId, model.deviceId).subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : Subscriber<LoginResponse>() {
                    override fun onCompleted() {}

                    override fun onError(e: Throwable) {
                        //dismissProgress();
                        // Toast.makeText(RegistrationActivity.this, e.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                        listener.onHomeFailed("Not able to connect")

                    }

                    override fun onNext(mLoginResponse: LoginResponse) {
                        //  dismissProgress();
                        if (mLoginResponse.status == 200) {
                            listener.onHomeSucess(mLoginResponse)
                        } else {
                            listener.onHomeFailed(mLoginResponse.error!!)
                        }


                    }

                })
    }


}
