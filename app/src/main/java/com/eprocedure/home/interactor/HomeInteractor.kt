package com.eprocedure.home.interactor

import com.eprocedure.login.model.request.LoginRequest
import com.eprocedure.login.model.response.LoginResponse

interface HomeInteractor{

    fun homeData(listener: OnFinishedListener, model: LoginRequest)
    fun getTermData(listener: OnFinishedListener, model: LoginRequest)

    interface OnFinishedListener {
        fun onHomeSucess(items: LoginResponse)

        fun onHomeFailed(errorMessage: String)

        fun onTermSucess(items: LoginResponse)

        fun onTermFailed(errorMessage: String)


    }
}
