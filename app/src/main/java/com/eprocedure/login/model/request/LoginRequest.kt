package com.eprocedure.login.model.request

/**
 * Created by Pawan on 10/30/2017.
 */

class LoginRequest {

    var userId: Int? = null
    var username: String? = null
    var deviceId: String? = null
    var term_id: Int? = null
    var search_text: String? = null
}
