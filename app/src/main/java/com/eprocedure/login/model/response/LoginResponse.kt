package com.eprocedure.login.model.response

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable
import java.util.*

class LoginResponse: Serializable {
    @SerializedName("content_data")
    @Expose
    var contentData: List<ContentDatum>? = null
    @SerializedName("data")
    @Expose
    var data: ArrayList<Datum>? = null
    @SerializedName("user_data")
    @Expose
    var userData: UserData? = null
    @SerializedName("content")
    @Expose
    var content: Int? = null
    @SerializedName("status")
    @Expose
    var status: Int? = null
    @SerializedName("error")
    @Expose
    var error: String? = null

}
