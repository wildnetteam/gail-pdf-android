package com.eprocedure.login.model.response

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable

class Datum :Serializable{

    @SerializedName("term_id")
    @Expose
    var termId: Int? = null
    @SerializedName("name")
    @Expose
    var name: String? = null
    @SerializedName("slug")
    @Expose
    var slug: String? = null
    @SerializedName("term_group")
    @Expose
    var termGroup: Int? = null
    @SerializedName("term_taxonomy_id")
    @Expose
    var termTaxonomyId: Int? = null
    @SerializedName("taxonomy")
    @Expose
    var taxonomy: String? = null
    @SerializedName("description")
    @Expose
    var description: String? = null
    @SerializedName("parent")
    @Expose
    var parent: Int? = null
    @SerializedName("count")
    @Expose
    var count: Int? = null
    @SerializedName("filter")
    @Expose
    var filter: String? = null
    @SerializedName("term_order")
    @Expose
    var termOrder: String? = null
    @SerializedName("show_button")
    @Expose
    var showButton: String? = null
    @SerializedName("updated")
    @Expose
    var updated: String? = null
    @SerializedName("child")
    @Expose
    var child: Int? = null
 @SerializedName("uploaded_pdf")
    @Expose
    var uploaded_pdf: String? = null

}
