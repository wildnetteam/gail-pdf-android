package com.eprocedure.login.model.response

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable

class UserData : Serializable {

    @SerializedName("user_id")
    @Expose
    var userId: Int? = null

}
