package com.eprocedure.login

import android.app.Activity
import android.app.ProgressDialog
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Toast
import com.eprocedure.R
import com.eprocedure.home.HomeActivity
import com.eprocedure.login.model.request.LoginRequest
import com.eprocedure.login.model.response.LoginResponse
import com.eprocedure.login.presenter.LoginPresenterImpl
import com.eprocedure.utility.PreferenceHelper
import com.eprocedure.utility.UIUtils
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : Activity(), ILoginView, View.OnClickListener {
    private var progress: ProgressDialog? = null
    private lateinit var mainPresenter: LoginPresenterImpl
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        mainPresenter = LoginPresenterImpl(this)
        signin_next.setOnClickListener(this)
        val userid = PreferenceHelper.getMyPrefString(this@LoginActivity, "user_id")
        val rememberMe = PreferenceHelper.getMyPrefBoolean(this@LoginActivity, "remember_me")
        if (rememberMe == true) {
            edittextusername.setText(userid)
            edittextusername.setSelection(edittextusername.length())
            remember_me_checkbox.isChecked = rememberMe
        }
    }

    override fun showProgress() {
        progress = ProgressDialog(this)
        progress!!.setMessage("Loading")
        progress!!.setCancelable(false)
        progress!!.show()
    }

    override fun hideProgress() {
        if (progress != null) {
            progress!!.dismiss()
        }
    }

    override fun setItems(items: LoginResponse) {
        PreferenceHelper.setMyPrefString(this@LoginActivity, "size", "")
        PreferenceHelper.setMyPrefString(this@LoginActivity, "user_id", edittextusername.text.toString().trim())
        PreferenceHelper.setMyPrefBoolean(this@LoginActivity, "remember_me", remember_me_checkbox.isChecked)
        val intent = Intent(applicationContext, HomeActivity::class.java)
        intent.putExtra("user_id", items.userData!!.userId)
        startActivity(intent)
        finish()
    }

    override fun onLoginFailure(failureMessage: String) {
        Toast.makeText(this, failureMessage, Toast.LENGTH_SHORT).show()
    }

    override fun onClick(v: View?) {
        if (UIUtils.isNetworkAvailable(this)) {
            if (edittextusername.text.trim().toString() != "") {
                val mLoginRequest = LoginRequest()
                mLoginRequest.username = edittextusername.text.trim().toString()
                mLoginRequest.deviceId = UIUtils.getUniqueId(this)
                mainPresenter.onLoginClick(mLoginRequest)
            } else {
                Toast.makeText(this, "Please enter employee Id", Toast.LENGTH_SHORT).show()
            }
        } else {
            Toast.makeText(this, "Please connect to internet.", Toast.LENGTH_SHORT).show()
        }
    }

}
