package com.eprocedure.login.interactor


import com.eprocedure.login.model.request.LoginRequest
import com.eprocedure.login.model.response.LoginResponse
import com.eprocedure.networking.retrofit.client.ApiFactory

import rx.Subscriber
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers

class LoginInteractorImpl : LoginInteractor {

    override fun userLogin(finishedListener: LoginInteractor.OnFinishedListener, model: LoginRequest) {
        ApiFactory.getClient().userLogin(model.username, model.deviceId).subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : Subscriber<LoginResponse>() {
                    override fun onCompleted() {}

                    override fun onError(e: Throwable) {
                        //dismissProgress();
                        // Toast.makeText(RegistrationActivity.this, e.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                        finishedListener.onLoginFailed("Not able to connect")

                    }

                    override fun onNext(mLoginResponse: LoginResponse) {
                        //  dismissProgress();


                        if (mLoginResponse.status == 200) {
                            finishedListener.onLoginSucess(mLoginResponse)
                        } else {
                            finishedListener.onLoginFailed(mLoginResponse.error!!)
                        }
                    }

                })
    }
}
