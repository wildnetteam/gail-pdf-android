package com.eprocedure.login.interactor


import com.eprocedure.login.model.request.LoginRequest
import com.eprocedure.login.model.response.LoginResponse

interface LoginInteractor {

    fun userLogin(listener: OnFinishedListener, model: LoginRequest)

    interface OnFinishedListener {
        fun onLoginSucess(items: LoginResponse)

        fun onLoginFailed(errorMessage: String)
    }
}
