package com.eprocedure.login.presenter


import com.eprocedure.login.model.request.LoginRequest

interface LoginPresenter {

    fun onLoginClick(mLoginRequest: LoginRequest)

    fun onDestroy()
}