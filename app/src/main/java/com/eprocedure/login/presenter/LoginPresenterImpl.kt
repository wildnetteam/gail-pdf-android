package com.eprocedure.login.presenter


import com.eprocedure.login.ILoginView
import com.eprocedure.login.interactor.LoginInteractor
import com.eprocedure.login.interactor.LoginInteractorImpl
import com.eprocedure.login.model.request.LoginRequest
import com.eprocedure.login.model.response.LoginResponse

/**
 * Created by Pawan on 10/30/2017.
 */

class LoginPresenterImpl(private val mILoginView: ILoginView?) : LoginPresenter, LoginInteractor.OnFinishedListener {
    private val mLoginInteractor: LoginInteractor

    init {
        this.mLoginInteractor = LoginInteractorImpl()

    }


    override fun onLoginClick(mLoginRequest: LoginRequest) {
        mILoginView!!.showProgress()
        mLoginInteractor.userLogin(this, mLoginRequest)
    }

    override fun onDestroy() {
        /* mLoginRouter.onDestroy();
        mILoginView = null;*/
    }

    override fun onLoginSucess(mLoginResponse: LoginResponse) {
        mILoginView?.hideProgress()
        mILoginView?.setItems(mLoginResponse)
    }

    override fun onLoginFailed(errorMessage: String) {
        if (mILoginView != null) {
            mILoginView.hideProgress()
            mILoginView.onLoginFailure(errorMessage)

        }
    }


}
