package com.eprocedure.login

import com.eprocedure.login.model.response.LoginResponse

interface ILoginView {

    fun showProgress()

    fun hideProgress()

    fun setItems(items: LoginResponse)

    fun onLoginFailure(failureMessage: String)

}
