package com.eprocedure.pdf

import android.app.Activity
import android.os.Bundle
import com.eprocedure.R
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_image_view.*


class ImageViewerActivity : Activity() {
    private var pdf_url: String? = ""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_image_view)
        pdf_url = intent.extras.getString("pdf_url")
        /* val uri = Uri.parse(pdf_url)
         fresco_view.setImageURI(uri,null)*/
        /** changes has been done by me
         *
         */
        try {

            Picasso.get()
                    .load(pdf_url)
                    .into(fresco_view)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }
}