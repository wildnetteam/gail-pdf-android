package com.eprocedure.pdf

import android.app.Activity
import android.app.ProgressDialog
import android.graphics.Bitmap
import android.os.Bundle
import android.view.View
import android.webkit.WebChromeClient
import android.webkit.WebView
import android.webkit.WebViewClient
import com.eprocedure.R
import com.eprocedure.button.IButtonView
import com.eprocedure.button.presenter.ButtonPresenterImpl
import com.eprocedure.content.controller.ContentAdapter
import com.eprocedure.login.model.response.Datum
import com.eprocedure.login.model.response.LoginResponse
import im.delight.android.webview.AdvancedWebView
import kotlinx.android.synthetic.main.activity_content.*
import java.util.*


/**
 * Created by Pawan on 8/31/2018.
 */

class LoadPdfFile : Activity(), AdvancedWebView.Listener, View.OnClickListener, IButtonView {
    private var pdf_url: String? = ""
    private var data: ArrayList<Datum>? = ArrayList()
    private var dataLogin: LoginResponse? = LoginResponse()
    private var progress: ProgressDialog? = null
    private var index:Int=0
    override fun showProgress() {
        progress=  ProgressDialog(this@LoadPdfFile)
        progress!!.setMessage("Please wait...")
        progress!!.isIndeterminate = false
        progress!!.setCancelable(false)
        progress!!.show()
    }

    override fun hideProgress() {
        if (progress!=null){
            progress!!.dismiss()
        }
    }
    override fun setItems(items: LoginResponse) {
        if (items.data!!.size>0){

            data!!.clear()
            data!!.addAll(items.data!!)
            adapter.notifyDataSetChanged()
        }else{
        }
    }

    override fun onHomeFailure(failureMessage: String) {
    }
    private lateinit var mainPresenter: ButtonPresenterImpl
    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.landing_content -> {
                webview.visibility= View.GONE
                content_rv.visibility= View.VISIBLE
            }
            R.id.landing_pdf -> {
            }
        }
    }
    private lateinit var adapter: ContentAdapter

    override fun onPageFinished(p0: String?) {
        System.out.println("page finish")
        ++index
        if (index>1){

            hideProgress()
        }
    }
    override fun onPageError(p0: Int, p1: String?, p2: String?) {
        hideProgress()

    }

    override fun onDownloadRequested(p0: String?, p1: String?, p2: String?, p3: Long, p4: String?, p5: String?) {
        webview.loadUrl("http://docs.google.com/gview?embedded=true&url=$p0")
    }

    override fun onExternalPageRequest(p0: String?) {
    }

    override fun onPageStarted(p0: String?, p1: Bitmap?) {
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.pdf_view)
        showProgress()
        dataLogin = intent.extras.getSerializable("data") as? LoginResponse
        pdf_url = intent.extras.getString("pdf_url")
        mainPresenter = ButtonPresenterImpl(this)
        webview.setListener(this, this)
        webview.setGeolocationEnabled(false)
        webview.setMixedContentAllowed(true)
        webview.setCookiesEnabled(true)
        webview.setThirdPartyCookiesEnabled(true)
        webview.webViewClient = object : WebViewClient() {
            override fun onPageFinished(view: WebView, url: String) {
                println("url is---$url")
            }
        }
        webview.webChromeClient = object : WebChromeClient() {
        }
        webview.addHttpHeader("X-Requested-With", "")
        webview.loadUrl("http://docs.google.com/gview?embedded=true&url=$pdf_url")
    }
}
