package com.eprocedure.faq.answer

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.support.annotation.RequiresApi
import android.view.KeyEvent
import android.view.View
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import android.widget.TextView
import com.eprocedure.R
import com.eprocedure.home.HomeActivity
import com.eprocedure.search.ActivitySearch
import com.eprocedure.utility.PreferenceHelper
import kotlinx.android.synthetic.main.activity_answer.*

class AnswerActivity : Activity() {
    private var user_id: Int = 0
    private var heading_name: String = ""
    private var pdf_url: String? = ""
    var parent_term_id: Int = 0
    @SuppressLint("SetJavaScriptEnabled")
    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_answer)
        parent_term_id = intent.extras.getInt("parent_term_id")
        heading_name = intent.extras.getString("heading_name")
        user_id = intent.extras.getInt("user_id")
        pdf_url = intent.extras.getString("pdf_url")
        val answers: String = intent.getStringExtra("answers")
        val question: String = intent.getStringExtra("question")
        val answerHtml = "<html><body style='text-align:justify;'>$answers</body></html>"
        val webSettings = answer_tv!!.settings
        val webSettingQuestion = question_tv!!.settings
        webSettings.javaScriptEnabled = true
        webSettingQuestion.javaScriptEnabled = true
        search_tv.visibility = View.INVISIBLE
        question_tv!!.loadData(question, "text/html", "UTF-8")
        answer_tv!!.loadData(answerHtml, "text/html", "UTF-8")
        val size = PreferenceHelper.getMyPrefString(this@AnswerActivity, "size")
        if (size.equals("") || size!!.toLowerCase() == "normal") {
            webSettings.defaultFontSize = 18
            webSettingQuestion.defaultFontSize = 18
        } else if (size.toLowerCase() == "large") {
            webSettings.defaultFontSize = 20
            webSettingQuestion.defaultFontSize = 20
        } else {
            webSettingQuestion.defaultFontSize = 16
        }


        back_tv.setOnClickListener {
            val intent = Intent(applicationContext, HomeActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            intent.putExtra("user_id", user_id)
            startActivity(intent)
            this@AnswerActivity.finishAffinity()
        }
        search_tv.setOnClickListener {
            if (search.visibility == View.VISIBLE) {
                search.visibility = View.GONE
                val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                imm.hideSoftInputFromWindow(search.windowToken, 0)
            } else {
                search.setText("")
                search.visibility = View.VISIBLE
                search.requestFocus()
                val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0)
            }
        }
        search.setOnEditorActionListener(object : TextView.OnEditorActionListener {
            override fun onEditorAction(v: TextView, actionId: Int, event: KeyEvent?): Boolean {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                    imm.hideSoftInputFromWindow(search.windowToken, 0)
                    search.visibility = View.GONE
                    val intent = Intent(applicationContext, ActivitySearch::class.java)
                    intent.putExtra("parent_term_id", parent_term_id)
                    intent.putExtra("user_id", user_id)
                    intent.putExtra("heading_name", heading_name)
                    intent.putExtra("search_text", search.text.toString())
                    startActivity(intent)
                    return true
                }
                return false
            }
        })
    }
}
