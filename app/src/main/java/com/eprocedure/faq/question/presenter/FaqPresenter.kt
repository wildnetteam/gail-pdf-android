package com.eprocedure.faq.question.presenter

import com.eprocedure.login.model.request.LoginRequest


interface FaqPresenter{
    fun getFaqData(mLoginRequest: LoginRequest)
    fun getFaqSearchData(mLoginRequest: LoginRequest)

    fun onDestroy()

}