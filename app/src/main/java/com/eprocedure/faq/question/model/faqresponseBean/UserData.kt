package com.eprocedure.faq.question.model.faqresponseBean

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class UserData {

    @SerializedName("user_id")
    @Expose
    var userId: Int? = null

}
