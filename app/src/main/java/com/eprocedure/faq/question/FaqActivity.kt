package com.eprocedure.faq.question

import android.app.Activity
import android.app.ProgressDialog
import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.speech.RecognizerIntent
import android.support.annotation.RequiresApi
import android.support.v7.widget.LinearLayoutManager
import android.view.KeyEvent
import android.view.View
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import android.widget.TextView
import com.eprocedure.faq.question.controller.QuestionAdapter
import com.eprocedure.faq.question.model.faqresponseBean.Datum
import com.eprocedure.faq.question.model.faqresponseBean.FaqResponseBean
import com.eprocedure.faq.question.presenter.FaqPresenterImpl
import com.eprocedure.home.HomeActivity
import com.eprocedure.login.model.request.LoginRequest
import com.eprocedure.utility.UIUtils
import kotlinx.android.synthetic.main.activity_faq.*
import java.util.*


class FaqActivity : Activity(), IFaqView {
    lateinit var mainPresenter: FaqPresenterImpl
    private var term_id: Int = 0
    var search_text: String = ""
    private lateinit var adapter: QuestionAdapter
    private var progress: ProgressDialog? = null
    var parent_term_id: Int = 0
    private val arrayList = ArrayList<Datum>()//Creating an empty arraylist
    var user_id: Int = 0
    var heading_name: String = ""
    var pdf_url: String? = ""
    override fun showProgress() {
        progress = ProgressDialog(this)
        progress!!.setMessage("Loading")
        progress!!.setCancelable(false)
        progress!!.show()
    }

    override fun hideProgress() {
        progress!!.dismiss()
    }

    override fun setItems(items: FaqResponseBean) {
        if (arrayList != null) {
            arrayList.clear()
            arrayList.addAll(items.data!!)
        }
        adapter.notifyDataSetChanged()
    }

    override fun onfaqFailure(failureMessage: String) {
    }


    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(com.eprocedure.R.layout.activity_faq)
        System.out.print("size is---" + arrayList.size)
        user_id = intent.extras.getInt("user_id")
        term_id = intent.extras.getInt("term_id")
        parent_term_id = intent.extras.getInt("parent_term_id")
        heading_name = intent.extras.getString("heading_name")
        pdf_url = intent.extras.getString("pdf_url")
        adapter = QuestionAdapter(arrayList, this@FaqActivity)
        val layoutManager = LinearLayoutManager(applicationContext)
        faq_ques_rv?.layoutManager = layoutManager
        faq_ques_rv?.adapter = adapter
        adapter.notifyDataSetChanged()
        back_tv.setOnClickListener {
            val intent = Intent(applicationContext, HomeActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            intent.putExtra("user_id", user_id)
            startActivity(intent)
            this@FaqActivity.finishAffinity()
        }

        mainPresenter = FaqPresenterImpl(this)
        val mLoginReuest = LoginRequest()
        mLoginReuest.deviceId = UIUtils.getUniqueId(this@FaqActivity)
        mLoginReuest.userId = user_id
        mLoginReuest.term_id = term_id
        mainPresenter.getFaqData(mLoginReuest)
        search_tv.setOnClickListener {

            if (search.visibility == View.VISIBLE) {
                search.visibility = View.GONE
                voice.visibility = View.GONE
                val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                imm.hideSoftInputFromWindow(search.windowToken, 0)
            } else {
                search.setText("")

                search.visibility = View.VISIBLE
                voice.visibility = View.VISIBLE
                search.requestFocus()
                val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0)
            }

        }

        search.setOnEditorActionListener(object : TextView.OnEditorActionListener {
            override fun onEditorAction(v: TextView, actionId: Int, event: KeyEvent?): Boolean {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    search.visibility = View.GONE
                    voice.visibility = View.GONE

                    search_text = search.text.toString()
                    val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                    imm.hideSoftInputFromWindow(search.windowToken, 0)
                    val mLoginReuest = LoginRequest()
                    mLoginReuest.deviceId = UIUtils.getUniqueId(this@FaqActivity)
                    mLoginReuest.userId = user_id
                    mLoginReuest.term_id = term_id
                    mLoginReuest.search_text = search.text.toString()
                    mainPresenter.getFaqSearchData(mLoginReuest)
                    return true
                }
                return false
            }
        })

        back_tv.setOnClickListener {

            val intent = Intent(applicationContext, HomeActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            intent.putExtra("user_id", user_id)

            startActivity(intent)
            this@FaqActivity.finishAffinity()

        }
        voice.setOnClickListener { askSpeechInput() }

    }

    private fun askSpeechInput() {
        val intent = Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH)
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
                RecognizerIntent.LANGUAGE_MODEL_FREE_FORM)
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault())
        intent.putExtra(RecognizerIntent.EXTRA_PROMPT,
                "Hi speak something")
        try {
            startActivityForResult(intent, 800)
        } catch (a: ActivityNotFoundException) {

        }

    }
}