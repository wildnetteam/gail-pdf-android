package com.eprocedure.faq.question.interactor

import com.eprocedure.faq.question.model.faqresponseBean.FaqResponseBean
import com.eprocedure.login.model.request.LoginRequest
import com.eprocedure.networking.retrofit.client.ApiFactory
import rx.Subscriber
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers

class FaqInteractorImpl : FaqInteractor {
    override fun faqDataSearch(listener: FaqInteractor.OnFinishedListener, model: LoginRequest) {
            ApiFactory.getClient().getfaqsearchData(model.userId, model.deviceId,model.term_id,model.search_text).subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(object : Subscriber<FaqResponseBean>() {
                        override fun onCompleted() {}

                        override fun onError(e: Throwable) {
                            //dismissProgress();
                            // Toast.makeText(RegistrationActivity.this, e.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                            listener.onfaqFailed("Not able to connect")

                        }

                        override fun onNext(mLoginResponse: FaqResponseBean) {
                            //  dismissProgress();


                            if (mLoginResponse.status == 200) {
                                listener.onfaqSucess(mLoginResponse)
                            } else {
                                listener.onfaqFailed("Data not found")
                            }
                        }

                    })


    }

    override fun faqData(listener: FaqInteractor.OnFinishedListener, model: LoginRequest) {
        ApiFactory.getClient().faqData(model.userId, model.deviceId,model.term_id).subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : Subscriber<FaqResponseBean>() {
                    override fun onCompleted() {}

                    override fun onError(e: Throwable) {
                        //dismissProgress();
                        // Toast.makeText(RegistrationActivity.this, e.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                        listener.onfaqFailed("Not able to connect")

                    }

                    override fun onNext(mLoginResponse: FaqResponseBean) {
                        //  dismissProgress();


                        if (mLoginResponse.status == 200) {
                            listener.onfaqSucess(mLoginResponse)
                        } else {
                            listener.onfaqFailed("Data not found")
                        }
                    }

                })

    }





}
