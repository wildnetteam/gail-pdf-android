package com.eprocedure.faq.question.interactor

import com.eprocedure.faq.question.model.faqresponseBean.FaqResponseBean
import com.eprocedure.login.model.request.LoginRequest


interface FaqInteractor{

    fun faqData(listener: OnFinishedListener, model: LoginRequest)
    fun faqDataSearch(listener: OnFinishedListener, model: LoginRequest)

    interface OnFinishedListener {
        fun onfaqSucess(items: FaqResponseBean)

        fun onfaqFailed(errorMessage: String)
    }
}
