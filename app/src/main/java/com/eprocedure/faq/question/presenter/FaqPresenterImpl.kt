package com.eprocedure.faq.question.presenter

import com.eprocedure.faq.question.IFaqView
import com.eprocedure.faq.question.interactor.FaqInteractor
import com.eprocedure.faq.question.interactor.FaqInteractorImpl
import com.eprocedure.faq.question.model.faqresponseBean.FaqResponseBean
import com.eprocedure.login.model.request.LoginRequest

class FaqPresenterImpl(private val mIFaqView: IFaqView?) : FaqPresenter, FaqInteractor.OnFinishedListener {
    override fun getFaqSearchData(mLoginRequest: LoginRequest) {
        mIFaqView!!.showProgress()
        mHomeInteractor.faqDataSearch(this, mLoginRequest)

    }

    private val mHomeInteractor: FaqInteractor

    init {
        this.mHomeInteractor = FaqInteractorImpl()

    }
    override fun getFaqData(mLoginRequest: LoginRequest) {

        mIFaqView!!.showProgress()
        mHomeInteractor.faqData(this, mLoginRequest)
    }

    override fun onDestroy() {
    }

    override fun onfaqSucess(items: FaqResponseBean) {

        mIFaqView?.hideProgress()
        mIFaqView?.setItems(items)

    }

    override fun onfaqFailed(errorMessage: String) {
        if (mIFaqView != null) {
            mIFaqView.hideProgress()
            mIFaqView.onfaqFailure(errorMessage)

        }
    }




}
