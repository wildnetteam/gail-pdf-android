package com.eprocedure.faq.question

import com.eprocedure.faq.question.model.faqresponseBean.FaqResponseBean

interface IFaqView {
    fun showProgress()
    fun hideProgress()
    fun setItems(items: FaqResponseBean)
    fun onfaqFailure(failureMessage: String)

}
