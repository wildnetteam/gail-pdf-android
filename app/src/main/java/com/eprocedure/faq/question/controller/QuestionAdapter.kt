package com.eprocedure.faq.question.controller

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.webkit.WebView
import com.eprocedure.faq.answer.AnswerActivity
import com.eprocedure.faq.question.FaqActivity
import com.eprocedure.faq.question.model.faqresponseBean.Datum
import com.eprocedure.utility.PreferenceHelper


class QuestionAdapter(private var items: ArrayList<Datum>, private val context: Context) : RecyclerView.Adapter<QuestionAdapter.ViewHolder>() {

    private val MAX_TOUCH_DURATION: Long = 100
    var m_DownTime: Long = 0
    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): ViewHolder {
        val itemView = LayoutInflater.from(parent.context)
                .inflate(com.eprocedure.R.layout.questions_item_view, parent, false)
        itemView.setOnClickListener {

        }
        return ViewHolder(itemView)

    }

    @SuppressLint("ClickableViewAccessibility", "SetJavaScriptEnabled")
    override fun onBindViewHolder(holder: ViewHolder, p1: Int) {
        val userDto = items[p1]
        val spannableTextView = "<html><body style='text-align:justify;'>" + "<Strong>Query:</Strong>" + userDto.question + "</body></html>"
        val webSettings = holder.question_tv!!.settings
        webSettings.javaScriptEnabled = true
        holder.question_tv!!.loadData(spannableTextView, "text/html", "UTF-8")


        val size = PreferenceHelper.getMyPrefString(context, "size")
        if (size.equals("") || size!!.toLowerCase() == "normal") {
            webSettings.defaultFontSize = 18
        } else if (size.toLowerCase() == "large") {
            webSettings.defaultFontSize = 20
        } else {
            webSettings.defaultFontSize = 16
        }
        holder.question_tv!!.setOnTouchListener(object : View.OnTouchListener {
            override fun onTouch(v: View?, event: MotionEvent?): Boolean {
                when (event?.action) {
                    MotionEvent.ACTION_DOWN -> {

                        m_DownTime = event.eventTime //init time

                    }

                    MotionEvent.ACTION_UP -> {
                        if (event.eventTime - m_DownTime <= MAX_TOUCH_DURATION) {
                            val intent = Intent(context, AnswerActivity::class.java)
                            intent.putExtra("answers", userDto.answer)
                            intent.putExtra("question", spannableTextView)
                            intent.putExtra("parent_term_id", ((context as FaqActivity)).parent_term_id)
                            intent.putExtra("user_id", (context).user_id)
                            intent.putExtra("heading_name", (context).heading_name)
                            context.startActivity(intent)

                        }
                    }
                }

                return v?.onTouchEvent(event) ?: true
            }
        })
    }

    override fun getItemCount(): Int {
        return items.size
    }


    class ViewHolder(row: View) : RecyclerView.ViewHolder(row) {


        var question_tv: WebView? = null

        init {
            this.question_tv = row.findViewById(com.eprocedure.R.id.question_tv)
        }
    }
}