package com.eprocedure.faq.question.model.faqresponseBean

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Datum {

    @SerializedName("question")
    @Expose
    var question: String? = null
    @SerializedName("answer")
    @Expose
    var answer: String? = null

}
