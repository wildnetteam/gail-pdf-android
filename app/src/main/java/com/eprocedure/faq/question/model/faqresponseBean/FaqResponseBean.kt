package com.eprocedure.faq.question.model.faqresponseBean

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class FaqResponseBean {

    @SerializedName("data")
    @Expose
    var data: List<Datum>? = null
    @SerializedName("user_data")
    @Expose
    var userData: UserData? = null
    @SerializedName("content")
    @Expose
    var content: Int? = null
    @SerializedName("status")
    @Expose
    var status: Int? = null

}
