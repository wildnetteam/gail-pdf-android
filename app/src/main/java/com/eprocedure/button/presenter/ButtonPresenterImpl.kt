package com.eprocedure.button.presenter

import com.eprocedure.button.IButtonView
import com.eprocedure.button.interactor.ButtonInteractor
import com.eprocedure.button.interactor.ButtonInteractorImpl
import com.eprocedure.login.model.request.LoginRequest
import com.eprocedure.login.model.response.LoginResponse

class ButtonPresenterImpl(private val mIHomeView: IButtonView?) : ButtonPresenter, ButtonInteractor.OnFinishedListener {
    private val mHomeInteractor: ButtonInteractor
    init {
        this.mHomeInteractor = ButtonInteractorImpl()
    }

    override fun getHomeData(mLoginRequest: LoginRequest) {
        mIHomeView!!.showProgress()
        mHomeInteractor.homeData(this, mLoginRequest)
    }

    override fun onDestroy() {
    }

    override fun onHomeSucess(items: LoginResponse) {
        mIHomeView?.hideProgress()
        mIHomeView?.setItems(items)
    }

    override fun onHomeFailed(errorMessage: String) {
        if (mIHomeView != null) {
            mIHomeView.hideProgress()
            mIHomeView.onHomeFailure(errorMessage)

        }
    }

}
