package com.eprocedure.button.presenter

import com.eprocedure.login.model.request.LoginRequest


interface ButtonPresenter{
    fun getHomeData(mLoginRequest: LoginRequest)
    fun onDestroy()
}