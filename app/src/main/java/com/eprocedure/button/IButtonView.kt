package com.eprocedure.button

import com.eprocedure.login.model.response.LoginResponse

interface IButtonView {

    fun showProgress()

    fun hideProgress()

    fun setItems(items: LoginResponse)

    fun onHomeFailure(failureMessage: String)

}
