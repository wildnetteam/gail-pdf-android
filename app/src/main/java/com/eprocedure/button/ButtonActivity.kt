package com.eprocedure.button

import android.app.Activity
import android.app.ProgressDialog
import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import com.eprocedure.R
import com.eprocedure.amendment.AmendmentActivity
import com.eprocedure.amendment.CircularActivity
import com.eprocedure.button.controller.ButtonAdapter
import com.eprocedure.button.presenter.ButtonPresenterImpl
import com.eprocedure.content.ContentActivity
import com.eprocedure.faq.question.FaqActivity
import com.eprocedure.login.model.request.LoginRequest
import com.eprocedure.login.model.response.Datum
import com.eprocedure.login.model.response.LoginResponse
import com.eprocedure.utility.UIUtils
import kotlinx.android.synthetic.main.activity_home.*

class ButtonActivity : Activity(), View.OnClickListener, IButtonView {
    private var progress: ProgressDialog? = null
    private var user_id: Int = 0
    var adapter: ButtonAdapter? = null
    private val homeDataList = ArrayList<Datum>()//Creating an empty arraylist
    lateinit var mainPresenter: ButtonPresenterImpl
    lateinit var headerString: String
    private var heading_name: String = ""
    private var parent_term_id: Int = 0
    override fun onClick(v: View?) {
    }

    override fun showProgress() {
        progress = ProgressDialog(this)
        progress!!.setMessage("Loading")
        progress!!.setCancelable(false)
        progress!!.show()
    }

    override fun hideProgress() {
        progress!!.dismiss()
    }

    override fun setItems(items: LoginResponse) {

        if (items.data != null) {

            if (items.data!![0].showButton.equals("1")) {
                val intent = Intent(applicationContext, ButtonActivity::class.java)
                intent.putExtra("data", items)
                intent.putExtra("pdf_url", pdf_url)
                intent.putExtra("user_id", user_id)
                intent.putExtra("heading_name", heading_name)
                intent.putExtra("parent_term_id", parent_term_id)

                startActivity(intent)
            } else {

                try {
                    val intent = Intent(applicationContext, ContentActivity::class.java)
                    intent.putExtra("data", items)
                    intent.putExtra("pdf_url", pdf_url)
                    intent.putExtra("user_id", user_id)
                    intent.putExtra("heading_name", heading_name)
                    intent.putExtra("parent_term_id", parent_term_id)

                    startActivity(intent)
                } catch (e: Exception) {
                    e.printStackTrace()
                }

            }

        } else {

            val intent = Intent(applicationContext, ContentActivity::class.java)
            intent.putExtra("data", items)
            intent.putExtra("user_id", user_id)

            startActivity(intent)
        }
    }

    override fun onHomeFailure(failureMessage: String) {
    }

    private var pdf_url: String? = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
        setAdapter()
        val homeData = intent.extras.getSerializable("data") as? LoginResponse
        pdf_url = intent.extras.getString("pdf_url")
        heading_name = intent.extras.getString("heading_name")
        user_id = intent.extras.getInt("user_id")
        parent_term_id = intent.extras.getInt("parent_term_id")
        homeDataList.addAll(homeData!!.data!!)
        adapter!!.notifyDataSetChanged()
        mainPresenter = ButtonPresenterImpl(this)
        bottom_ll.visibility = View.INVISIBLE
    }
    private fun setAdapter() {
        adapter = ButtonAdapter(homeDataList, this@ButtonActivity, object : ButtonAdapter.ItemClickInterfaces {
            override fun onitemClick(mdatum: Datum) {
                when {
                    mdatum.slug.equals("amendments") -> {
                        val intent = Intent(applicationContext, AmendmentActivity::class.java)
                        intent.putExtra("term_id", mdatum.termId)
                        intent.putExtra("pdf_url", pdf_url)
                        intent.putExtra("user_id", user_id)
                        intent.putExtra("parent_term_id", parent_term_id)
                        intent.putExtra("heading_name", heading_name)

                        startActivity(intent)
                    }
                    mdatum.slug.equals("circulars") -> {
                        val intent = Intent(applicationContext, CircularActivity::class.java)
                        intent.putExtra("term_id", mdatum.termId)
                        intent.putExtra("pdf_url", pdf_url)
                        intent.putExtra("user_id", user_id)
                        intent.putExtra("parent_term_id", parent_term_id)
                        intent.putExtra("heading_name", heading_name)

                        startActivity(intent)
                    }
                    mdatum.slug.equals("faqs") -> {
                        val intent = Intent(applicationContext, FaqActivity::class.java)
                        intent.putExtra("term_id", mdatum.termId)
                        intent.putExtra("pdf_url", pdf_url)
                        intent.putExtra("user_id", user_id)
                        intent.putExtra("parent_term_id", parent_term_id)
                        intent.putExtra("heading_name", heading_name)

                        startActivity(intent)
                    }
                    else -> {
                        heading_name = mdatum.name!!
                        parent_term_id = mdatum.termId!!
                        //    Toast.makeText(this@ButtonActivity, "clicked id is--" + mdatum.name, Toast.LENGTH_SHORT).show()
                        val mLoginReuest = LoginRequest()
                        mLoginReuest.deviceId = UIUtils.getUniqueId(this@ButtonActivity)
                        mLoginReuest.userId = user_id
                        mLoginReuest.term_id = mdatum.termId
                        headerString = mdatum.name!!
                        mainPresenter.getHomeData(mLoginReuest)
                    }
                }
            }
        })
        val layoutManager = LinearLayoutManager(applicationContext)
        home_rv?.layoutManager = layoutManager
        home_rv?.adapter = adapter
        adapter!!.notifyDataSetChanged()
    }
}