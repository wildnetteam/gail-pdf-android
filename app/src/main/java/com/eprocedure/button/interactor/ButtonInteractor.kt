package com.eprocedure.button.interactor

import com.eprocedure.login.model.request.LoginRequest
import com.eprocedure.login.model.response.LoginResponse

interface ButtonInteractor{

    fun homeData(listener: OnFinishedListener, model: LoginRequest)

    interface OnFinishedListener {
        fun onHomeSucess(items: LoginResponse)

        fun onHomeFailed(errorMessage: String)
    }
}
