package com.eprocedure.amendment

import android.app.Activity
import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.support.annotation.RequiresApi
import android.support.v7.widget.LinearLayoutManager
import android.view.KeyEvent
import android.view.View
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import android.widget.TextView
import com.eprocedure.R
import com.eprocedure.amendment.controller.AmendmentAdapter
import com.eprocedure.amendment.model.amendmentResponseBean.Amendment
import com.eprocedure.amendment.model.amendmentResponseBean.AmendmentResponse
import com.eprocedure.amendment.presenter.AmendmentPresenterImpl
import com.eprocedure.home.HomeActivity
import com.eprocedure.login.model.request.LoginRequest
import com.eprocedure.search.ActivitySearch
import com.eprocedure.utility.UIUtils
import kotlinx.android.synthetic.main.activity_amendment.*
import java.util.*

class AmendmentActivity : Activity(), IAmendmentView {
    private lateinit var mainPresenter: AmendmentPresenterImpl
    private val arrayList = ArrayList<Amendment>()//Creating an empty arraylist
    private var term_id: Int = 0
    private var progress: ProgressDialog? = null

    private lateinit var adapter: AmendmentAdapter
    private var user_id: Int = 0
    var heading_name: String = ""
    var pdf_url: String? = ""
    var parent_term_id: Int = 0
    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_amendment)
        user_id = intent.extras.getInt("user_id")
        term_id = intent.extras.getInt("term_id")
        parent_term_id = intent.extras.getInt("parent_term_id")
        heading_name = intent.extras.getString("heading_name")
        pdf_url = intent.extras.getString("pdf_url")
        adapter = AmendmentAdapter(arrayList, this@AmendmentActivity)
        search_tv.visibility=View.GONE

        title_tv.text=getString(R.string.amendment)
        val layoutManager = LinearLayoutManager(applicationContext)
        amendment_rv?.layoutManager = layoutManager
        amendment_rv?.adapter = adapter
        adapter.notifyDataSetChanged()
        back_tv.setOnClickListener {  val intent = Intent(applicationContext, HomeActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            intent.putExtra("user_id", user_id)

            startActivity(intent)
            this@AmendmentActivity.finishAffinity() }
        mainPresenter = AmendmentPresenterImpl(this)
        getAmmendment()
        search_tv.setOnClickListener {

            if (search.visibility == View.VISIBLE){
                search.visibility = View.GONE
                val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                imm.hideSoftInputFromWindow(search.windowToken, 0)
            }else{
                search.setText("")

                search.visibility = View.VISIBLE
                search.requestFocus()
                val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0)
            }

        }

        search.setOnEditorActionListener(object : TextView.OnEditorActionListener {
            override fun onEditorAction(v: TextView, actionId: Int, event: KeyEvent?): Boolean {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                    imm.hideSoftInputFromWindow(search.windowToken, 0)
                    search.visibility = View.GONE
                    val intent = Intent(applicationContext, ActivitySearch::class.java)
                    intent.putExtra("parent_term_id", parent_term_id)
                    intent.putExtra("user_id", user_id)
                    intent.putExtra("heading_name", heading_name)
                    intent.putExtra("search_text", search.text.toString())

                    startActivity(intent)

                    return true
                }
                return false
            }
        })
    }
    private fun getAmmendment() {
        val mLoginReuest = LoginRequest()
        mLoginReuest.deviceId = UIUtils.getUniqueId(this@AmendmentActivity)
        mLoginReuest.userId = user_id
        mLoginReuest.term_id = term_id
        mainPresenter.getAmendment(mLoginReuest)
    }
    override fun showProgress() {
        progress = ProgressDialog(this)
        progress!!.setMessage("Loading")
        progress!!.setCancelable(false)
        progress!!.show()
    }

    override fun hideProgress() {
        if (progress!=null){
            progress!!.dismiss()
        }
    }
    override fun setItems(items: AmendmentResponse) {
        arrayList.addAll(items.data!!)
        adapter.notifyDataSetChanged()
    }
    override fun onAmendmentFailure(failureMessage: String) {
    }

}