package com.eprocedure.amendment.model.amendmentResponseBean

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class AmendmentResponse {

    @SerializedName("data")
    @Expose
    var data: List<Amendment>? = null
    @SerializedName("user_data")
    @Expose
    var userData: UserData? = null
    @SerializedName("content")
    @Expose
    var content: Int? = null
    @SerializedName("status")
    @Expose
    var status: Int? = null
 @SerializedName("error")
    @Expose
    var error: String? = null

}
