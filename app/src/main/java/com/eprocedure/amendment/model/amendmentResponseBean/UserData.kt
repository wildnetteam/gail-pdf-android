package com.eprocedure.amendment.model.amendmentResponseBean

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class UserData {

    @SerializedName("user_id")
    @Expose
    var userId: Int? = null

}
