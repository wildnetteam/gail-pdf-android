package com.eprocedure.amendment.interactor

import com.eprocedure.amendment.model.amendmentResponseBean.AmendmentResponse
import com.eprocedure.login.model.request.LoginRequest
import com.eprocedure.networking.retrofit.client.ApiFactory
import rx.Subscriber
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers

class AmendmentInteractorImpl : AmendmentInteractor {

    override fun getAmendment(finishedListener: AmendmentInteractor.OnFinishedListener, model: LoginRequest) {
        ApiFactory.getClient().getAmendment(model.userId, model.deviceId,model.term_id).subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : Subscriber<AmendmentResponse>() {
                    override fun onCompleted() {}

                    override fun onError(e: Throwable) {
                        //dismissProgress();
                        // Toast.makeText(RegistrationActivity.this, e.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                        finishedListener.onAmendmentFailed("Not able to connect")

                    }

                    override fun onNext(mLoginResponse: AmendmentResponse) {
                        //  dismissProgress();


                        if (mLoginResponse.status == 200) {
                            finishedListener.onAmendmentSucess(mLoginResponse)
                        } else {
                            finishedListener.onAmendmentFailed(mLoginResponse.error!!)
                        }
                    }

                })
    }override fun getCircular(finishedListener: AmendmentInteractor.OnFinishedListener, model: LoginRequest) {
        ApiFactory.getClient().getCircular(model.userId, model.deviceId,model.term_id).subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : Subscriber<AmendmentResponse>() {
                    override fun onCompleted() {}

                    override fun onError(e: Throwable) {
                        //dismissProgress();
                        // Toast.makeText(RegistrationActivity.this, e.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                        finishedListener.onAmendmentFailed("Not able to connect")

                    }

                    override fun onNext(mLoginResponse: AmendmentResponse) {
                        //  dismissProgress();


                        if (mLoginResponse.status == 200) {
                            finishedListener.onAmendmentSucess(mLoginResponse)
                        } else {
                            finishedListener.onAmendmentFailed(mLoginResponse.error!!)
                        }
                    }

                })
    }
}
