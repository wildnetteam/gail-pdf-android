package com.eprocedure.amendment.interactor

import com.eprocedure.amendment.model.amendmentResponseBean.AmendmentResponse
import com.eprocedure.login.model.request.LoginRequest

interface AmendmentInteractor {

    fun getAmendment(listener: OnFinishedListener, model: LoginRequest)
    fun getCircular(listener: OnFinishedListener, model: LoginRequest)

    interface OnFinishedListener {
        fun onAmendmentSucess(items: AmendmentResponse)

        fun onAmendmentFailed(errorMessage: String)
    }
}