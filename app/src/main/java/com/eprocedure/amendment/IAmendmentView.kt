package com.eprocedure.amendment

import com.eprocedure.amendment.model.amendmentResponseBean.AmendmentResponse

interface IAmendmentView {

    fun showProgress()

    fun hideProgress()

    fun setItems(items: AmendmentResponse)

    fun onAmendmentFailure(failureMessage: String)

}