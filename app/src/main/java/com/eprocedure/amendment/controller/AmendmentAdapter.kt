package com.eprocedure.amendment.controller

import android.content.Context
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.TypedValue
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.eprocedure.R
import com.eprocedure.amendment.model.amendmentResponseBean.Amendment
import com.eprocedure.utility.PreferenceHelper

class AmendmentAdapter(private var items: ArrayList<Amendment>, val context: Context) : RecyclerView.Adapter<AmendmentAdapter.ViewHolder>(), View.OnClickListener {
    override fun onClick(v: View?) {
        when (v!!.id) {
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): ViewHolder {
        val itemView = LayoutInflater.from(parent.context)
                .inflate(R.layout.amendment_item_view, parent, false)
        return ViewHolder(itemView)

    }

    override fun onBindViewHolder(holder: ViewHolder, p1: Int) {
        val userDto = items[p1]
        holder.date_tv!!.text = userDto.amendmentCircularsDate
        holder.ref_no_tv!!.text = userDto.referenceNo
        holder.subject!!.text = userDto.postTitle
        holder.refrence_no_tv!!.text = userDto.postContent


        val size = PreferenceHelper.getMyPrefString(context, "size")
        if (size.equals("") || size!!.toLowerCase() == "normal") {
            holder.date_tv!!.setTextSize(TypedValue.COMPLEX_UNIT_SP, 18f)
            holder.ref_no_tv!!.setTextSize(TypedValue.COMPLEX_UNIT_SP, 18f)
            holder.subject!!.setTextSize(TypedValue.COMPLEX_UNIT_SP, 18f)
            holder.refrence_no_tv!!.setTextSize(TypedValue.COMPLEX_UNIT_SP, 18f)
        } else if (size.toLowerCase() == "large") {
            holder.date_tv!!.setTextSize(TypedValue.COMPLEX_UNIT_SP, 20f)
            holder.ref_no_tv!!.setTextSize(TypedValue.COMPLEX_UNIT_SP, 20f)
            holder.subject!!.setTextSize(TypedValue.COMPLEX_UNIT_SP, 20f)
            holder.refrence_no_tv!!.setTextSize(TypedValue.COMPLEX_UNIT_SP, 20f)
        } else {
            holder.date_tv!!.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16f)
            holder.ref_no_tv!!.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16f)
            holder.subject!!.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16f)
            holder.refrence_no_tv!!.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16f)
        }
        val itemListDataAdapter = ViewPdfAdapter(userDto.uploadedFile!!, context)

        holder.pdf_rv?.layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
        holder.pdf_rv?.adapter = itemListDataAdapter

    }

    override fun getItemCount(): Int {
        return items.size
    }

    class ViewHolder(row: View) : RecyclerView.ViewHolder(row) {
        var date_tv: TextView? = null
        var ref_no_tv: TextView? = null
        var subject: TextView? = null
        var refrence_no_tv: TextView? = null
        var pdf_rv: RecyclerView? = null

        init {
            this.date_tv = row.findViewById(R.id.date_tv)
            this.ref_no_tv = row.findViewById(R.id.ref_no_tv)
            this.subject = row.findViewById(R.id.subject)
            this.refrence_no_tv = row.findViewById(R.id.refrence_no_tv)
            this.pdf_rv = row.findViewById(R.id.pdf_rv)
        }
    }
}