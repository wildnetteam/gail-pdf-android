package com.eprocedure.amendment.controller

import android.content.Context
import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.eprocedure.R
import com.eprocedure.pdf.ImageViewerActivity
import com.eprocedure.pdf.LoadPdfFile

class ViewPdfAdapter(private var items: ArrayList<String>, val context: Context) : RecyclerView.Adapter<ViewPdfAdapter.ViewHolder>(), View.OnClickListener {
    override fun onClick(v: View?) {
        when (v!!.id) {
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): ViewHolder {
        val itemView = LayoutInflater.from(parent.context)
                .inflate(R.layout.pdf_item_view, parent, false)
        return ViewHolder(itemView)

    }
    override fun onBindViewHolder(holder: ViewHolder, p1: Int) {
        val userDto = items[p1]

        holder.pdf_view!!.text=context.getString(R.string.view_file)
        holder.pdf_view!!.setOnClickListener{
            if (userDto.toLowerCase().trim().contains(".pdf")){
                val intent = Intent(context, LoadPdfFile::class.java)
                intent.putExtra("pdf_url",userDto)
                context.startActivity(intent)
            }else{
                val intent = Intent(context, ImageViewerActivity::class.java)
                intent.putExtra("pdf_url", userDto)
                context.startActivity(intent)
            }
        }

    }
    override fun getItemCount(): Int {
        return items.size
    }
    class ViewHolder(row: View) : RecyclerView.ViewHolder(row) {

        var pdf_view: TextView? = null

        init {

            this.pdf_view= row.findViewById(R.id.pdf_view)
        }
    }
}