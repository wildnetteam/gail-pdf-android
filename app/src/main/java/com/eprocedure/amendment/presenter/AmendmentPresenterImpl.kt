package com.eprocedure.amendment.presenter

import com.eprocedure.amendment.IAmendmentView
import com.eprocedure.amendment.interactor.AmendmentInteractor
import com.eprocedure.amendment.interactor.AmendmentInteractorImpl
import com.eprocedure.amendment.model.amendmentResponseBean.AmendmentResponse
import com.eprocedure.login.model.request.LoginRequest

class AmendmentPresenterImpl(private val mIAmendmentView: IAmendmentView?) : AmendmentPresenter, AmendmentInteractor.OnFinishedListener {
    override fun getCircular(mLoginRequest: LoginRequest) {

        mIAmendmentView!!.showProgress()
        mAmendmentInteractor.getCircular(this, mLoginRequest)
    }

    override fun getAmendment(mLoginRequest: LoginRequest) {
        mIAmendmentView!!.showProgress()
        mAmendmentInteractor.getAmendment(this, mLoginRequest)

    }


    private val mAmendmentInteractor: AmendmentInteractor

    init {
        this.mAmendmentInteractor = AmendmentInteractorImpl()

    }


    override fun onDestroy() {
        /* mLoginRouter.onDestroy();
        mILoginView = null;*/
    }

    override fun onAmendmentSucess(mAmendmentResponse: AmendmentResponse) {
        mIAmendmentView?.hideProgress()
        mIAmendmentView?.setItems(mAmendmentResponse)
    }

    override fun onAmendmentFailed(errorMessage: String) {
        if (mIAmendmentView != null) {
            mIAmendmentView.hideProgress()
            mIAmendmentView.onAmendmentFailure(errorMessage)

        }
    }


}
