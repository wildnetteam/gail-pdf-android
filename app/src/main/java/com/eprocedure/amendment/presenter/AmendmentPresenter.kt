package com.eprocedure.amendment.presenter

import com.eprocedure.login.model.request.LoginRequest

interface AmendmentPresenter {

    fun getAmendment(mLoginRequest: LoginRequest)
    fun getCircular(mLoginRequest: LoginRequest)

    fun onDestroy()
}