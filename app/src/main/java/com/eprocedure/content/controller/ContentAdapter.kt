package com.eprocedure.content.controller

import android.content.Context
import android.graphics.Color
import android.support.v7.widget.RecyclerView
import android.text.Html
import android.util.TypedValue
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.eprocedure.R
import com.eprocedure.login.model.response.Datum
import com.eprocedure.utility.PreferenceHelper
import java.util.*

class ContentAdapter(private var items: ArrayList<Datum>, val context: Context, private val btnlistener: ItemClickInterfaces) : RecyclerView.Adapter<ContentAdapter.ViewHolder>() {
    companion object {
        var itemClickListener: ItemClickInterfaces? = null
    }

    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): ViewHolder {
        val itemView = LayoutInflater.from(parent.context)
                .inflate(R.layout.content_item_view, parent, false)

        return ViewHolder(itemView)

    }

    override fun onBindViewHolder(holder: ViewHolder, p1: Int) {
        itemClickListener = btnlistener

        val userDto = items[p1]
        if (userDto.updated.equals("1")) {
            holder.home_list_title?.setTextColor(Color.parseColor("#FF0000"))
        }else{
            holder.home_list_title?.setTextColor(Color.parseColor("#000000"))
        }


        val size = PreferenceHelper.getMyPrefString(context,"size")
        if (size.equals("") || size!!.toLowerCase() == "normal"){
            holder.home_list_title!!.setTextSize(TypedValue.COMPLEX_UNIT_SP,18f)
        }else if (size.toLowerCase() == "large"){
            holder.home_list_title!!.setTextSize(TypedValue.COMPLEX_UNIT_SP,20f)
        }else{
            holder.home_list_title!!.setTextSize(TypedValue.COMPLEX_UNIT_SP,16f)
        }

        holder.home_list_title?.text = Html.fromHtml(userDto.name)
        holder.home_list_title?.setOnClickListener { itemClickListener?.onitemClick(userDto,p1) }
    }

    override fun getItemCount(): Int {
        return items.size
    }


    class ViewHolder(row: View) : RecyclerView.ViewHolder(row) {
        var home_list_title: TextView? = null

        init {
            this.home_list_title = row.findViewById(com.eprocedure.R.id.home_list_title)
        }
    }

    interface ItemClickInterfaces { // create an interface
        fun onitemClick(mdatum: Datum,position:Int)  // create callback function
    }
}