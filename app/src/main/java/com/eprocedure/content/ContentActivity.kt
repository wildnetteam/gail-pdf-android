package com.eprocedure.content

import android.app.Activity
import android.app.ProgressDialog
import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.os.Build
import android.os.Bundle
import android.speech.RecognizerIntent
import android.support.annotation.RequiresApi
import android.support.v7.widget.LinearLayoutManager
import android.util.TypedValue
import android.view.KeyEvent
import android.view.View
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import android.webkit.WebChromeClient
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.TextView
import com.eprocedure.R
import com.eprocedure.button.IButtonView
import com.eprocedure.button.presenter.ButtonPresenterImpl
import com.eprocedure.content.controller.ContentAdapter
import com.eprocedure.home.HomeActivity
import com.eprocedure.login.model.request.LoginRequest
import com.eprocedure.login.model.response.Datum
import com.eprocedure.login.model.response.LoginResponse
import com.eprocedure.pdf.LoadPdfFile
import com.eprocedure.search.ActivitySearch
import com.eprocedure.utility.PreferenceHelper
import com.eprocedure.utility.UIUtils
import im.delight.android.webview.AdvancedWebView
import kotlinx.android.synthetic.main.activity_content.*
import java.util.*

class ContentActivity : Activity(), AdvancedWebView.Listener, View.OnClickListener, IButtonView {
    private var user_id: Int = 0
    private var position: Int = 0
    private var progress: ProgressDialog? = null
    private var heading_name: String = ""
    private var parent_term_id: Int = 0

    private var data: ArrayList<Datum>? = ArrayList()
    private var dataLogin: LoginResponse? = LoginResponse()
    private var pdf_url: String? = ""
    private var header: String? = ""
    override fun showProgress() {
        progress = ProgressDialog(this)
        progress!!.setMessage("Loading")
        progress!!.setCancelable(false)
        progress!!.show()
    }

    override fun hideProgress() {
        progress!!.dismiss()
    }

    override fun setItems(items: LoginResponse) {
        if (items.data!!.size > 0) {
            data!!.clear()
            data!!.addAll(items.data!!)
            adapter.notifyDataSetChanged()
        } else {
            val intent = Intent(applicationContext, ContentDetail::class.java)
            intent.putExtra("data", items)
            intent.putExtra("header", header)
            intent.putExtra("all_data", data)
            intent.putExtra("user_id", user_id)
            intent.putExtra("pdf_url", pdf_url)
            intent.putExtra("heading_name", heading_name)
            intent.putExtra("parent_term_id", parent_term_id)
            intent.putExtra("search_text", "")
            intent.putExtra("position", position)
            startActivityForResult(intent, 500)
        }
    }

    override fun onHomeFailure(failureMessage: String) {
    }

    lateinit var mainPresenter: ButtonPresenterImpl

    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.landing_content -> {
                if (content_rv.visibility == View.INVISIBLE || content_rv.visibility == View.GONE) {
                    webview.visibility = View.GONE
                    content_rv.visibility = View.VISIBLE
                } else {
                    val mLoginReuest = LoginRequest()
                    mLoginReuest.deviceId = UIUtils.getUniqueId(this@ContentActivity)
                    mLoginReuest.userId = user_id
                    mLoginReuest.term_id = parent_term_id
                    mainPresenter.getHomeData(mLoginReuest)
                }
            }
            R.id.landing_pdf -> {
                val intent = Intent(applicationContext, LoadPdfFile::class.java)
                intent.putExtra("pdf_url", pdf_url)
                startActivity(intent)
            }
        }
    }

    private lateinit var adapter: ContentAdapter

    override fun onPageFinished(p0: String?) {
    }

    override fun onPageError(p0: Int, p1: String?, p2: String?) {
    }

    override fun onDownloadRequested(p0: String?, p1: String?, p2: String?, p3: Long, p4: String?, p5: String?) {
        webview.loadUrl("http://docs.google.com/gview?embedded=true&url=$p0")
    }

    override fun onExternalPageRequest(p0: String?) {
    }

    override fun onPageStarted(p0: String?, p1: Bitmap?) {
    }

    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_content)
        dataLogin = intent.extras.getSerializable("data") as? LoginResponse
        header = intent.extras.getString("header")
        user_id = intent.extras.getInt("user_id")
        heading_name = intent.extras.getString("heading_name")
        pdf_url = intent.extras.getString("pdf_url")
        parent_term_id = intent.extras.getInt("parent_term_id")

        mainPresenter = ButtonPresenterImpl(this)
        heading_tv.text = heading_name
        webview.setListener(this, this)
        webview.setGeolocationEnabled(false)
        webview.setMixedContentAllowed(true)
        webview.setCookiesEnabled(true)
        webview.setThirdPartyCookiesEnabled(true)

        val size = PreferenceHelper.getMyPrefString(this@ContentActivity, "size")
        if (size.equals("") || size!!.toLowerCase() == "normal") {
            webview.settings.defaultFontSize = 18
            heading_tv!!.setTextSize(TypedValue.COMPLEX_UNIT_SP, 18f)
        } else if (size.toLowerCase() == "large") {
            webview.settings.defaultFontSize = 20
            heading_tv!!.setTextSize(TypedValue.COMPLEX_UNIT_SP, 20f)
        } else {
            webview.settings.defaultFontSize = 16
            heading_tv!!.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16f)
        }
        webview.webViewClient = object : WebViewClient() {
            override fun onPageFinished(view: WebView, url: String) {
                println("url is---$url")
            }
        }
        webview.webChromeClient = object : WebChromeClient() {
        }
        webview.addHttpHeader("X-Requested-With", "")
        landing_content.setOnClickListener(this)
        landing_pdf.setOnClickListener(this)


        if (dataLogin!!.contentData!!.isNotEmpty()) {
            val content = "<html><body style='text-align:justify;'>" + dataLogin!!.contentData!![0].postContent + "</body></html>"
            webview.loadDataWithBaseURL(null, content, "text/html", "UTF-8", null)
            webview.visibility = View.VISIBLE
            content_rv.visibility = View.GONE
        } else {
            webview.visibility = View.GONE
            content_rv.visibility = View.VISIBLE
        }
        data?.addAll(dataLogin!!.data!!)

        search_tv.setOnClickListener {

            if (search.visibility == View.VISIBLE) {
                search.visibility = View.GONE
                voice.visibility = View.GONE

                val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                imm.hideSoftInputFromWindow(search.windowToken, 0)
            } else {
                search.setText("")
                voice.visibility = View.VISIBLE

                search.visibility = View.VISIBLE
                search.requestFocus()
                val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0)
            }
        }

        search.setOnEditorActionListener(object : TextView.OnEditorActionListener {
            override fun onEditorAction(v: TextView, actionId: Int, event: KeyEvent?): Boolean {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                    imm.hideSoftInputFromWindow(search.windowToken, 0)
                    search.visibility = View.GONE
                    voice.visibility = View.GONE

                    val intent = Intent(applicationContext, ActivitySearch::class.java)
                    intent.putExtra("parent_term_id", parent_term_id)
                    intent.putExtra("user_id", user_id)
                    intent.putExtra("heading_name", heading_name)
                    intent.putExtra("search_text", search.text.toString())

                    startActivity(intent)

                    return true
                }
                return false
            }
        })
        back_tv.setOnClickListener {

            val intent = Intent(applicationContext, HomeActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            intent.putExtra("user_id", user_id)
            startActivity(intent)
            this@ContentActivity.finishAffinity()
        }

        setAdapter()
        voice.setOnClickListener { askSpeechInput() }

    }

    private fun setAdapter() {
        adapter = ContentAdapter(data!!, this@ContentActivity, object : ContentAdapter.ItemClickInterfaces {
            override fun onitemClick(mdatum: Datum, posi: Int) {
                position = posi
                header = mdatum.name
                val mLoginReuest = LoginRequest()
                mLoginReuest.deviceId = UIUtils.getUniqueId(this@ContentActivity)
                mLoginReuest.userId = user_id
                mLoginReuest.term_id = mdatum.termId
                mainPresenter.getHomeData(mLoginReuest)
            }
        })
        val layoutManager = LinearLayoutManager(applicationContext)
        content_rv?.layoutManager = layoutManager
        content_rv?.adapter = adapter
        adapter.notifyDataSetChanged()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            500 -> {
                val mLoginReuest = LoginRequest()
                mLoginReuest.deviceId = UIUtils.getUniqueId(this@ContentActivity)
                mLoginReuest.userId = user_id
                mLoginReuest.term_id = parent_term_id
                mainPresenter.getHomeData(mLoginReuest)
            }
            800 -> {
                if (resultCode == RESULT_OK) {
                    data?.let {
                        val results = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS)
                        search.setText(results[0])
                        val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                        imm.hideSoftInputFromWindow(search.windowToken, 0)
                        search.visibility = View.GONE
                        voice.visibility = View.GONE
                        val intent = Intent(applicationContext, ActivitySearch::class.java)
                        intent.putExtra("parent_term_id", parent_term_id)
                        intent.putExtra("user_id", user_id)
                        intent.putExtra("heading_name", heading_name)
                        intent.putExtra("search_text", search.text.toString())
                        startActivity(intent)
                    }
                }
            }
        }
    }

    private fun askSpeechInput() {
        val intent = Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH)
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
                RecognizerIntent.LANGUAGE_MODEL_FREE_FORM)
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault())
        intent.putExtra(RecognizerIntent.EXTRA_PROMPT,
                "Hi speak something")
        try {
            startActivityForResult(intent, 800)
        } catch (a: ActivityNotFoundException) {
        }
    }
}
