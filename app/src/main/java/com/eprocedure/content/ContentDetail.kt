package com.eprocedure.content

import android.annotation.SuppressLint
import android.annotation.TargetApi
import android.app.Activity
import android.app.ProgressDialog
import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.os.Build
import android.os.Bundle
import android.speech.RecognizerIntent
import android.support.annotation.RequiresApi
import android.text.Html
import android.util.TypedValue
import android.view.KeyEvent
import android.view.View
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import android.webkit.*
import android.widget.TextView
import android.widget.Toast
import com.eprocedure.R
import com.eprocedure.button.ButtonActivity
import com.eprocedure.button.IButtonView
import com.eprocedure.button.presenter.ButtonPresenterImpl
import com.eprocedure.home.HomeActivity
import com.eprocedure.login.model.request.LoginRequest
import com.eprocedure.login.model.response.Datum
import com.eprocedure.login.model.response.LoginResponse
import com.eprocedure.pdf.LoadPdfFile
import com.eprocedure.search.ActivitySearch
import com.eprocedure.utility.PreferenceHelper
import com.eprocedure.utility.UIUtils
import im.delight.android.webview.AdvancedWebView
import kotlinx.android.synthetic.main.activity_content_detail.*
import java.util.*


class ContentDetail : Activity(), AdvancedWebView.Listener, View.OnClickListener, IButtonView {
    private var pdf_url: String? = ""
    private var heading_name: String = ""
    private var search_text: String = ""
    private var parent_term_id: Int = 0
    private var progress: ProgressDialog? = null

    private var all_data: ArrayList<Datum>? = ArrayList()
    private lateinit var header: String
    private var currentIndex: Int = 0
    private var user_id: Int = 0
    private var dataLogin: LoginResponse? = LoginResponse()
    override fun showProgress() {
        progress = ProgressDialog(this)
        progress!!.setMessage("Loading")
        progress!!.setCancelable(false)
        progress!!.show()
    }

    override fun hideProgress() {
        if (progress != null) {
            progress!!.dismiss()
        }
    }


    override fun setItems(items: LoginResponse) {

        if (items.contentData!!.isNotEmpty()) {

            // val content = "<html><head><style>.tableblack td {border: 1px solid black;}.redtext{color:red;}.tabletext td {color: #ff0000; border: 1px solid black;}.highlight_text{background-color: #FF0; padding:3px;font-weight: bold;}</style></head><body style='text-align:justify;'>" + items!!.contentData!![0].postContent + "</body></html>"
            val content = "<html><head><style>.tableblack td {border: 1px solid black;}.redtext{color:red;}.tabletext td {color: #ff0000; border: 1px solid black;}.highlight_text{background-color: #FF0; padding:3px;font-weight: bold;}.table-responsive{overflow-x: auto; overflow-y: auto;}</style></head><body style='text-align:justify;'>" + items.contentData!![0].postContent + "</body></html>"

            webview.loadDataWithBaseURL(null, content.replace("https:","http:"), "text/html", "UTF-8", null)

        } else {
            if (items.data!![0].showButton.equals("1")) {
                val intent = Intent(applicationContext, ButtonActivity::class.java)
                intent.putExtra("data", items)
                intent.putExtra("pdf_url", pdf_url)
                intent.putExtra("user_id", user_id)
                intent.putExtra("heading_name", heading_name)
                intent.putExtra("parent_term_id", parent_term_id)

                startActivity(intent)
            } else {

                try {
                    val intent = Intent(applicationContext, ContentActivity::class.java)
                    intent.putExtra("data", items)
                    intent.putExtra("pdf_url", pdf_url)
                    intent.putExtra("user_id", user_id)
                    intent.putExtra("heading_name", heading_name)
                    intent.putExtra("parent_term_id", parent_term_id)

                    startActivity(intent)
                } catch (e: Exception) {
                    e.printStackTrace()
                }

            }
        }

    }

    override fun onHomeFailure(failureMessage: String) {
    }

    private lateinit var mainPresenter: ButtonPresenterImpl
    override fun onClick(v: View?) {

        when (v!!.id) {
            com.eprocedure.R.id.previous -> {
                if (currentIndex > 0) {
                    --currentIndex
                    html_heading.text = Html.fromHtml(all_data!![currentIndex].name)
                    next.visibility = View.VISIBLE
                    previous.visibility = View.VISIBLE
                    val mLoginReuest = LoginRequest()
                    mLoginReuest.deviceId = UIUtils.getUniqueId(this@ContentDetail)
                    mLoginReuest.userId = user_id
                    mLoginReuest.term_id = all_data!![currentIndex].termId
                    mainPresenter.getHomeData(mLoginReuest)

                    if (currentIndex == 0) {
                        previous.visibility = View.INVISIBLE
                        next.visibility = View.VISIBLE
                    }
                } else {
                    previous.visibility = View.INVISIBLE
                    next.visibility = View.VISIBLE
                }
            }
            R.id.next -> {
                if (currentIndex < all_data!!.size - 1) {
                    ++currentIndex
                    next.visibility = View.VISIBLE
                    previous.visibility = View.VISIBLE
                    val mLoginReuest = LoginRequest()
                    html_heading.text = Html.fromHtml(all_data!![currentIndex].name)
                    mLoginReuest.deviceId = UIUtils.getUniqueId(this@ContentDetail)
                    mLoginReuest.userId = user_id
                    mLoginReuest.term_id = all_data!![currentIndex].termId
                    mainPresenter.getHomeData(mLoginReuest)
                    if (currentIndex == all_data!!.size - 1) {
                        next.visibility = View.INVISIBLE
                        previous.visibility = View.VISIBLE
                    }
                } else {
                    next.visibility = View.INVISIBLE
                    previous.visibility = View.VISIBLE
                }
            }
        }
    }

    override fun onPageFinished(p0: String?) {

        webview.scrollTo(0, 0)
        nested_scroll.scrollTo(0, 0)

    }

    override fun onPageError(p0: Int, p1: String?, p2: String?) {
    }

    override fun onDownloadRequested(p0: String?, p1: String?, p2: String?, p3: Long, p4: String?, p5: String?) {
        webview.loadUrl("http://docs.google.com/gview?embedded=true&url=$p0")
    }

    override fun onExternalPageRequest(p0: String?) {
    }

    override fun onPageStarted(p0: String?, p1: Bitmap?) {
    }


    @SuppressLint("SetJavaScriptEnabled")
    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_content_detail)
        dataLogin = intent.extras.getSerializable("data") as? LoginResponse
        header = intent.extras.getString("header")
        all_data = intent.extras.getSerializable("all_data") as? ArrayList<Datum>
        user_id = intent.extras.getInt("user_id")
        currentIndex = intent.extras.getInt("position")
        pdf_url = intent.extras.getString("pdf_url")
        heading_name = intent.extras.getString("heading_name")
        parent_term_id = intent.extras.getInt("parent_term_id")
        search_text = intent?.extras?.getString("search_text")!!
        previous.visibility = View.GONE
        next.visibility = View.GONE
        if (search_text.trim().isNotEmpty()) {
            search_text = ""
            previous.visibility = View.GONE
            next.visibility = View.GONE
            bottom_pdf_ll.visibility = View.INVISIBLE
            previous_btn.visibility = View.GONE
            next_btn.visibility = View.GONE
        } else {
            previous_btn.visibility = View.GONE
        }
        mainPresenter = ButtonPresenterImpl(this)
        heading_tv.text = heading_name
        html_heading.text = dataLogin!!.contentData!![0].postTitle
        webview.setListener(this, this)
        webview.setGeolocationEnabled(false)
        webview.setMixedContentAllowed(true)
        webview.setCookiesEnabled(true)
        // Set web view client

        val size = PreferenceHelper.getMyPrefString(this@ContentDetail, "size")
        if (size.equals("") || size!!.toLowerCase() == "normal") {


            webview.settings.defaultFontSize = 18
            heading_tv!!.setTextSize(TypedValue.COMPLEX_UNIT_SP, 18f)
            html_heading!!.setTextSize(TypedValue.COMPLEX_UNIT_SP, 18f)

        } else if (size.toLowerCase() == "large") {

            webview.settings.defaultFontSize = 20
            heading_tv!!.setTextSize(TypedValue.COMPLEX_UNIT_SP, 20f)
            html_heading!!.setTextSize(TypedValue.COMPLEX_UNIT_SP, 20f)

        } else {

            webview.settings.defaultFontSize = 16
            heading_tv!!.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16f)
            html_heading!!.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16f)

        }
        webview.webViewClient = object : WebViewClient() {
            override fun onPageStarted(view: WebView, url: String, favicon: Bitmap?) {
                // Page loading started
                // Do something
            }

            override fun onPageFinished(view: WebView, url: String) {
                // Page loading finished
                // Display the loaded page title in a toast message
            }
        }


        // Set web view chrome client
        webview.webChromeClient = object : WebChromeClient() {
            override fun onProgressChanged(view: WebView, newProgress: Int) {
            }
        }

        webview.settings.javaScriptEnabled = true

        webview.settings.domStorageEnabled = true
        webview.settings.loadsImagesAutomatically = true
        webview.settings.mixedContentMode = WebSettings.MIXED_CONTENT_ALWAYS_ALLOW

        webview.setThirdPartyCookiesEnabled(true)
        webview.webViewClient = object : WebViewClient() {
            override fun onPageFinished(view: WebView, url: String) {
                println("url is---$url")
            }

            @TargetApi(Build.VERSION_CODES.M)
            @RequiresApi(Build.VERSION_CODES.M)
            override fun onReceivedError(view: WebView?, request: WebResourceRequest?, error: WebResourceError?) {
                if (error != null) {
                    println("Error is  is---${error.description}")
                }

                super.onReceivedError(view, request, error)
            }
        }
        webview.webChromeClient = object : WebChromeClient() {
        }
        webview.addHttpHeader("X-Requested-With", "")
        val content = "<html><head><style>.tableblack td {border: 1px solid black;}.redtext{color:red;}.tabletext td {color: #ff0000; border: 1px solid black;}.highlight_text{background-color: #FF0; padding:3px;font-weight: bold;}.table-responsive{overflow-x: auto; overflow-y: auto;}</style></head><body style='text-align:justify;'>" + dataLogin!!.contentData!![0].postContent + "</body></html>"
        webview.loadDataWithBaseURL(null, content, "text/html", "UTF-8", null)
        next.setOnClickListener(this)
        previous.setOnClickListener(this)

        landing_content.setOnClickListener {
            val output = Intent()
            setResult(Activity.RESULT_OK, output)
            finish()
        }
        back_tv.setOnClickListener {

            val intent = Intent(applicationContext, HomeActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            intent.putExtra("user_id", user_id)
            startActivity(intent)
            this@ContentDetail.finishAffinity()
        }
        landing_pdf.setOnClickListener {
            val intent = Intent(applicationContext, LoadPdfFile::class.java)
            intent.putExtra("pdf_url", pdf_url)
            startActivity(intent)
        }
        search_tv.setOnClickListener {

            if (search.visibility == View.VISIBLE) {
                search.visibility = View.GONE
                voice.visibility = View.GONE
                val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                imm.hideSoftInputFromWindow(search.windowToken, 0)
            } else {
                search.setText("")

                search.visibility = View.VISIBLE
                voice.visibility = View.VISIBLE
                search.requestFocus()
                val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0)
            }

        }

        search.setOnEditorActionListener(object : TextView.OnEditorActionListener {
            override fun onEditorAction(v: TextView, actionId: Int, event: KeyEvent?): Boolean {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                    imm.hideSoftInputFromWindow(search.windowToken, 0)
                    search.visibility = View.GONE
                    voice.visibility = View.GONE
                    val intent = Intent(applicationContext, ActivitySearch::class.java)
                    intent.putExtra("parent_term_id", parent_term_id)
                    intent.putExtra("user_id", user_id)
                    intent.putExtra("heading_name", heading_name)
                    intent.putExtra("search_text", search.text.toString())

                    startActivity(intent)

                    return true
                }
                return false
            }
        })

        /* webview.setOnTouchListener(new OnSwipeTouchListener(this@ContentDetail) {
             public void onSwipeTop() {
             }
             public void onSwipeRight() {
             }
             public void onSwipeLeft() {
             }

             public void onSwipeBottom() {
             }

         });*/

        previous_btn.setOnClickListener {

            if (!(search_text.trim().isNotEmpty())) {
                --currentIndex
                html_heading.text = Html.fromHtml(all_data!![currentIndex].name)
                next_btn.visibility = View.VISIBLE
                previous_btn.visibility = View.VISIBLE
                val mLoginReuest = LoginRequest()
                mLoginReuest.deviceId = UIUtils.getUniqueId(this@ContentDetail)
                mLoginReuest.userId = user_id
                mLoginReuest.term_id = all_data!![currentIndex].termId
                mainPresenter.getHomeData(mLoginReuest)
                if (currentIndex == 0) {
                    previous_btn.visibility = View.INVISIBLE
                    next_btn.visibility = View.VISIBLE
                    Toast.makeText(this@ContentDetail, "Previous not available!", Toast.LENGTH_SHORT).show()
                }
            }
        }


        next_btn.setOnClickListener {
            if (!(search_text.trim().isNotEmpty())) {
                if (!(search_text.trim().isNotEmpty())) {
                    if (currentIndex < all_data!!.size - 1) {
                        ++currentIndex
                        next_btn.visibility = View.VISIBLE
                        previous_btn.visibility = View.VISIBLE
                        val mLoginReuest = LoginRequest()
                        html_heading.text = Html.fromHtml(all_data!![currentIndex].name)
                        mLoginReuest.deviceId = UIUtils.getUniqueId(this@ContentDetail)
                        mLoginReuest.userId = user_id
                        mLoginReuest.term_id = all_data!![currentIndex].termId
                        mainPresenter.getHomeData(mLoginReuest)
                        if (currentIndex == all_data!!.size - 1) {
                            next_btn.visibility = View.INVISIBLE
                            previous_btn.visibility = View.VISIBLE
                            Toast.makeText(this@ContentDetail, "Next not available!", Toast.LENGTH_SHORT).show()
                        }
                    } else {
                        Toast.makeText(this@ContentDetail, "Next not available!", Toast.LENGTH_SHORT).show()
                        next_btn.visibility = View.INVISIBLE
                        previous_btn.visibility = View.VISIBLE
                    }
                }

            }
        }

        /*webview.setOnTouchListener(object : OnSwipeTouchListener(this@ContentDetail) {

            override fun onSwipeRight() {
                run {
                    if (!(search_text.trim().isNotEmpty())) {
                        --currentIndex
                        html_heading.text = Html.fromHtml(all_data!![currentIndex].name)
                        next.visibility = View.VISIBLE
                        previous.visibility = View.VISIBLE
                        var mLoginReuest = LoginRequest()
                        mLoginReuest.deviceId = UIUtils.getUniqueId(this@ContentDetail)
                        mLoginReuest.userId = user_id
                        mLoginReuest.term_id = all_data!![currentIndex].termId
                        mainPresenter.getHomeData(mLoginReuest)
                        if (currentIndex == 0) {
                            previous.visibility = View.INVISIBLE
                            next.visibility = View.VISIBLE
                            Toast.makeText(this@ContentDetail, "Previous not available!", Toast.LENGTH_SHORT).show()
                        }
                    }
                }
            }

            override fun onSwipeLeft() {
                run {
                    if (!(search_text.trim().isNotEmpty())) {
                        if (currentIndex < all_data!!.size - 1) {
                            ++currentIndex
                            next.visibility = View.VISIBLE
                            previous.visibility = View.VISIBLE
                            var mLoginReuest = LoginRequest()
                            html_heading.text = Html.fromHtml(all_data!![currentIndex].name)
                            mLoginReuest.deviceId = UIUtils.getUniqueId(this@ContentDetail)
                            mLoginReuest.userId = user_id
                            mLoginReuest.term_id = all_data!![currentIndex].termId
                            mainPresenter.getHomeData(mLoginReuest)
                            if (currentIndex == all_data!!.size - 1) {
                                next.visibility = View.INVISIBLE
                                previous.visibility = View.VISIBLE
                                Toast.makeText(this@ContentDetail, "Next not available!", Toast.LENGTH_SHORT).show()
                            }
                        } else {
                            Toast.makeText(this@ContentDetail, "Next not available!", Toast.LENGTH_SHORT).show()
                            next.visibility = View.INVISIBLE
                            previous.visibility = View.VISIBLE
                        }
                    }
                }
            }

        })*/

        voice.setOnClickListener { askSpeechInput() }
    }

    private fun askSpeechInput() {
        val intent = Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH)
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
                RecognizerIntent.LANGUAGE_MODEL_FREE_FORM)
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault())
        intent.putExtra(RecognizerIntent.EXTRA_PROMPT,
                "Hi speak something")
        try {
            startActivityForResult(intent, 800)
        } catch (a: ActivityNotFoundException) {

        }

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 800) {
            if (resultCode == RESULT_OK) {
                data?.let {
                    val results = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS)
                    search.setText(results[0])
                    val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                    imm.hideSoftInputFromWindow(search.windowToken, 0)
                    search.visibility = View.GONE
                    voice.visibility = View.GONE
                    val intent = Intent(applicationContext, ActivitySearch::class.java)
                    intent.putExtra("parent_term_id", parent_term_id)
                    intent.putExtra("user_id", user_id)
                    intent.putExtra("heading_name", heading_name)
                    intent.putExtra("search_text", search.text.toString())
                    startActivity(intent)
                }
            }
        }
    }
}
